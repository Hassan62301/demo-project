package com.kazyony.demo.core.model;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuditModel.class)
public abstract class AuditModel_ {

	public static volatile SingularAttribute<AuditModel, LocalDateTime> createdDate;
	public static volatile SingularAttribute<AuditModel, String> createdBy;
	public static volatile SingularAttribute<AuditModel, LocalDateTime> lastModifiedDate;
	public static volatile SingularAttribute<AuditModel, String> lastModifiedBy;
	public static volatile SingularAttribute<AuditModel, Integer> version;

}

