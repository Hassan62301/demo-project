package com.kazyony.demo.role.entity;

import com.kazyony.demo.permission.entity.Permission;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.entity.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Role.class)
public abstract class Role_ extends com.kazyony.demo.core.model.AuditModel_ {

	public static volatile SetAttribute<Role, Permission> permissions;
	public static volatile SingularAttribute<Role, String> name;
	public static volatile SingularAttribute<Role, String> description;
	public static volatile SingularAttribute<Role, Long> id;
	public static volatile SingularAttribute<Role, UserType> userType;
	public static volatile ListAttribute<Role, User> users;

}

