package com.kazyony.demo.user.entity;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserActivityLog.class)
public abstract class UserActivityLog_ {

	public static volatile SingularAttribute<UserActivityLog, String> performedBy;
	public static volatile SingularAttribute<UserActivityLog, LocalDateTime> performedAt;
	public static volatile SingularAttribute<UserActivityLog, Long> id;
	public static volatile SingularAttribute<UserActivityLog, ActivityType> activityType;

}

