package com.kazyony.demo.user.entity;

import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.user.constants.UserType;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ extends com.kazyony.demo.core.model.AuditModel_ {

	public static volatile SingularAttribute<User, String> firstName;
	public static volatile SingularAttribute<User, String> lastName;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Role> role;
	public static volatile SingularAttribute<User, Boolean> initialPassword;
	public static volatile ListAttribute<User, UserActivityLog> userActivityLogs;
	public static volatile SingularAttribute<User, Long> id;
	public static volatile SingularAttribute<User, UserType> userType;
	public static volatile SingularAttribute<User, String> userName;
	public static volatile SingularAttribute<User, Boolean> enabled;

}

