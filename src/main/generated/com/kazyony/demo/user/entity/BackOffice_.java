package com.kazyony.demo.user.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BackOffice.class)
public abstract class BackOffice_ extends com.kazyony.demo.user.entity.User_ {

	public static volatile SingularAttribute<BackOffice, String> emailAddress;

}

