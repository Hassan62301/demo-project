package com.kazyony.demo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Hassan Wael.
 */
@Component
public class PasswordEncodingHelper {
    private static final Logger logger = LoggerFactory.getLogger(PasswordEncodingHelper.class);

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    public String encodePasswordUsingBcrypt(String rawPassword) {
        logger.info("--## encodePasswordUsingBcrypt: encoding password using Bcrypt ##--");
        return bCryptPasswordEncoder.encode(rawPassword);
    }
}
