package com.kazyony.demo.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * @author Hassan Wael.
 */
public class PageRequestBuilder {

    /**
     * @param pageNumber
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @return
     */
    public static PageRequest build(Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder){
        Sort sort = Sort.by("id").descending();
        if (pageNumber == null) {
            pageNumber = 0;
        }
        if (pageSize == null) {
            pageSize = Integer.MAX_VALUE;
        }
        if (sortField != null && !sortField.equals("undefined") && !sortField.equals("null")) {
            sort = Sort.by(sortField);
            if (sortOrder == -1){
                sort = sort.descending();
            }
        }
        return PageRequest.of(pageNumber, pageSize, sort);
    }
}
