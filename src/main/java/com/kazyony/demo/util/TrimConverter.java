package com.kazyony.demo.util;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Hassan Wael.
 */
@Component
public class TrimConverter {
    private static final Logger logger = LoggerFactory.getLogger(TrimConverter.class);

    public Converter<String, String> trimData()
    {
        logger.info("--## Triming Data: Trimming string data ##--");
        Converter<String, String> toTrim = new AbstractConverter<String, String>() {
            protected String convert(String source) {
                return source == null ? null : source.trim();
            }
        };
        return toTrim;
    }
}
