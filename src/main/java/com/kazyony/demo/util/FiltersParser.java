package com.kazyony.demo.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kazyony.demo.core.model.FilterDto;
import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author agamal on 9/1/2020
 */
public class FiltersParser {

    public static List<FilterDto> parseFilters(String filters) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<FilterDto> filterDtoList = new ArrayList<>();

        JSONArray jsonArray = new JSONArray(filters);
        for (Object jsonObject : jsonArray) {
            FilterDto filterDto = mapper.readValue(jsonObject.toString(), FilterDto.class);
            filterDtoList.add(filterDto);
        }
        return filterDtoList;
    }
}
