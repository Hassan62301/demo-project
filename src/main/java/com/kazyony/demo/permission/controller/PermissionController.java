package com.kazyony.demo.permission.controller;

import com.kazyony.demo.core.errorhandling.ErrorResponse;
import com.kazyony.demo.core.jwt.exception.AuthenticationExceptionMessages;
import com.kazyony.demo.permission.constants.PermissionsLiterals;
import com.kazyony.demo.permission.dto.PermissionDto;
import com.kazyony.demo.permission.service.PermissionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Hassan Wael.
 */
@RestController
@RequestMapping("/v1/users/permissions")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /* Api endpoint to get all permissions in database */
    @GetMapping
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_ALL_ACTIONS})
    @ApiOperation(value = "This endpoint is for getting all permission", response = PermissionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = PermissionDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class)
    })
    public ResponseEntity<?> getAllPermissions() {

        List<PermissionDto> permissionDtoList = permissionService.findAllPermissionsDtos();
        return new ResponseEntity<>(permissionDtoList, HttpStatus.OK);
    }
}
