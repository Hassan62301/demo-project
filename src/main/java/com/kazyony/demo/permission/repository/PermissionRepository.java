package com.kazyony.demo.permission.repository;

import com.kazyony.demo.permission.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    /* find Permission by permission name */
    Permission findByName(String name);
}
