package com.kazyony.demo.permission.mapper;

import com.kazyony.demo.permission.dto.PermissionDto;
import com.kazyony.demo.permission.entity.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hassan Wael
 */
@Component
public class PermissionMapper {

    private static final Logger logger = LoggerFactory.getLogger(PermissionMapper.class);

    /* convert Permission Entity to PermissionDto */
    public PermissionDto convertPermissionToPermissionDto(Permission permission) {
        PermissionDto permissionDto = new PermissionDto();

        permissionDto.setId(permission.getId());
        permissionDto.setDescription(permission.getDescription());
        permissionDto.setName(permission.getName());

        logger.info("--## PermissionDto now is set in mapping ##--");
        return permissionDto;
    }

    /* convert Permission Dto to Permission Entity */
    public Permission convertPermissionDtoToPermission(PermissionDto permissionDto) {
        Permission permission = new Permission();

        permission.setId(permissionDto.getId());
        permission.setDescription(permissionDto.getDescription());
        permission.setName(permissionDto.getName());

        logger.info("--## Permission now is set in mapping ##--");
        return permission;
    }

    /* convert List of Permission Entities to List of Permission Dtos */
    public List<PermissionDto> convertListPermissionToDto(List<Permission> permissions) {
        List<PermissionDto> permissionDtos = new ArrayList<>();

        for (Permission permission : permissions) {
            PermissionDto permissionDto = convertPermissionToPermissionDto(permission);
            permissionDtos.add(permissionDto);
        }

        logger.info("--## permissionDtos now is set in mapping ##--");
        return permissionDtos;
    }

    /* convert List of Permission Dtos to List of Permission Entities */
    public List<Permission> convertListPermissionDtoToEntity(List<PermissionDto> permissionDtos) {
        List<Permission> permissions = new ArrayList<>();

        for (PermissionDto permissionDto : permissionDtos) {
            Permission permission = convertPermissionDtoToPermission(permissionDto);
            permissions.add(permission);
        }

        logger.info("--## permissions now is set in mapping ##--");
        return permissions;
    }
}
