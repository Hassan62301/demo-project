package com.kazyony.demo.permission.constants;

/* Contains all permissions names needed for the app */
public class PermissionsLiterals {

    /* Super User Permissions */
    public static final String ALL_PAGES_ALL_ACTIONS = "ALL_PAGES_ALL_ACTIONS";

    /* Role Permissions */
    public static final String ROLE_VIEW = "ROLE_VIEW";
    public static final String ROLE_VIEW_AND_ADD = "ROLE_VIEW_AND_ADD";
    public static final String ROLE_VIEW_AND_UPDATE = "ROLE_VIEW_AND_UPDATE";
    public static final String ROLE_VIEW_AND_DELETE = "ROLE_VIEW_AND_DELETE";
    public static final String ROLE_ALL_ACTIONS = "ROLE_ALL_ACTIONS";

    /* BackOffice Permissions */
    public static final String BACKOFFICE_VIEW = "BACKOFFICE_VIEW";
    public static final String BACKOFFICE_VIEW_AND_ADD = "BACKOFFICE_VIEW_AND_ADD";
    public static final String BACKOFFICE_VIEW_AND_UPDATE = "BACKOFFICE_VIEW_AND_UPDATE";
    public static final String BACKOFFICE_ALL_ACTIONS = "BACKOFFICE_ALL_ACTIONS";

    /* Common */
    public static final String USER_CHANGE_STATUS = "USER_CHANGE_STATUS";

}
