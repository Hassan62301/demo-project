package com.kazyony.demo.permission.service;

import com.kazyony.demo.permission.dto.PermissionDto;
import com.kazyony.demo.permission.entity.Permission;
import com.kazyony.demo.permission.mapper.PermissionMapper;
import com.kazyony.demo.permission.repository.PermissionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Hassan Wael
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    private static final Logger logger = LoggerFactory.getLogger(PermissionServiceImpl.class);

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private PermissionMapper mapper;

    /* get all permissions */
    @Override
    public List<PermissionDto> findAllPermissionsDtos() {

        List<Permission> permissionList = permissionRepository.findAll();
        List<PermissionDto> permissionDtos = mapper.convertListPermissionToDto(permissionList);

        logger.info("--## permissionDtos now is returned ##--");
        return permissionDtos;
    }
}
