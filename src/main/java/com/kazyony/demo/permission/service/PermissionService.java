package com.kazyony.demo.permission.service;

import com.kazyony.demo.permission.dto.PermissionDto;

import java.util.List;

/**
 * @author Hassan Wael
 */
public interface PermissionService {
    /* find all permissions in database */
    List<PermissionDto> findAllPermissionsDtos();
}
