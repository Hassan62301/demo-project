package com.kazyony.demo.permission.entity;

import javax.persistence.*;

/**
 * @author Hassan Wael
 */
@Entity
@Table(name = Permission.TABLE_NAME)
public class Permission {

    static final String TABLE_NAME = "PERMISSION";

    @Id
    @Column(name = "ID", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    public Permission() {
    }

    public Permission(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Permission [id=" + id + ", name=" + name + ", description=" + description + "]";
    }
}
