package com.kazyony.demo.constants;

/**
 * @author Hassan Wael.
 */
public class LoyaltyConstants {
    // constant date time format
    public static final String HOST_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

    // constant date format
    public static final String HOST_DATE_FORMAT = "yyyy-MM-dd";

    // constant time format
    public static final String HOST_TIME_FORMAT = "hh:mm a";
}
