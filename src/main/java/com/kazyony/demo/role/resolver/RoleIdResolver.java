package com.kazyony.demo.role.resolver;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdResolver;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.role.mapper.RoleMapper;
import com.kazyony.demo.role.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Hassan Wael
 */
@Component
public class RoleIdResolver implements ObjectIdResolver {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public void bindItem(ObjectIdGenerator.IdKey idKey, Object o) {
    }

    @Override
    public Object resolveId(ObjectIdGenerator.IdKey idKey) {
        Role role = this.roleRepository.findByName(idKey.key.toString());
        return roleMapper.convertRoleToDto(role);
    }

    @Override
    public ObjectIdResolver newForDeserialization(Object o) {
        return this;
    }

    @Override
    public boolean canUseFor(ObjectIdResolver objectIdResolver) {
        return false;
    }
}
