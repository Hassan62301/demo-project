package com.kazyony.demo.role.mapper;


import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.user.dto.CreateRoleRequestDto;
import com.kazyony.demo.util.TrimConverter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Hassan Wael
 */
@Component
public class RoleMapper {

    @Autowired
    TrimConverter trimConverter;
    @Autowired
    private ModelMapper modelMapper;

    /* convert Role Entity to Role Dto */
    public RoleDto convertRoleToDto(Role role) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(role, RoleDto.class);
    }

    /* convert Role Dto to Role Entity */
    public Role convertRoleToEntity(RoleDto roleDto) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(roleDto, Role.class);
    }
    /* convert Role Entity to CreateRoleRequestDto */
    public CreateRoleRequestDto convertCreateRoleToDto(Role role) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(role, CreateRoleRequestDto.class);
    }

    /* convert CreateRoleRequestDto to Entity */
    public Role convertRoleToEntity(CreateRoleRequestDto roleDto) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(roleDto, Role.class);
    }

    /* convert List of Role Entities to List of Role Dtos */
    public List<RoleDto> convertRoleListToDto(List<Role> roles) {
        modelMapper.addConverter(trimConverter.trimData());
        return roles.stream()
                .map(role -> modelMapper.map(role, RoleDto.class))
                .collect(Collectors.toList());
    }

    /* convert List of Role Dtos to List of Role Entities */
    public List<Role> convertRoleListToEntity(List<RoleDto> roles) {
        modelMapper.addConverter(trimConverter.trimData());
        return roles.stream()
                .map(role -> modelMapper.map(role, Role.class))
                .collect(Collectors.toList());
    }
}
