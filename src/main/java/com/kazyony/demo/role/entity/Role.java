package com.kazyony.demo.role.entity;

import com.kazyony.demo.core.model.AuditModel;
import com.kazyony.demo.permission.entity.Permission;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.entity.User;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Hassan Wael
 */
@Entity
@Table(name = Role.TABLE_NAME)
@TypeDef(name = "user_type", typeClass = PostgreSQLEnumType.class, defaultForType = UserType.class)
public class Role extends AuditModel {

    static final String TABLE_NAME = "ROLE";

    @Id
    @Column(name = "ID", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Type(type = "user_type")
    @Enumerated(EnumType.STRING)
    @Column(name = "USER_TYPE", length = 20, nullable = false)
//    @Column(name = "USER_TYPE", columnDefinition = "user_type", length = 20, nullable = false)
    private UserType userType;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "role")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ROLE_PERMISSION",
            joinColumns = @JoinColumn(name = "ROLE_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID")
    )
    @Fetch(value = FetchMode.SUBSELECT)
    private Set<Permission> permissions;

    public Role() {
        users = new ArrayList<>();
        permissions = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", userType=" + userType +
//                ", users=" + users +
//                ", permissions=" + permissions +
                '}';
    }
}
