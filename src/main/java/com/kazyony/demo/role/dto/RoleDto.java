package com.kazyony.demo.role.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.kazyony.demo.core.model.AuditModelDto;
import com.kazyony.demo.permission.dto.PermissionDto;
import com.kazyony.demo.role.resolver.RoleIdResolver;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.dto.UserDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Hassan Wael
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "name",
        resolver = RoleIdResolver.class, scope = RoleDto.class)
public class RoleDto extends AuditModelDto {

    private Long id;

    @NotBlank
    private String description;

    @NotBlank
    private String name;

    @NotNull
    private UserType userType;

    @JsonIgnore
    private List<UserDto> users;

    private Set<PermissionDto> permissions;

    /* Constructors */

    public RoleDto() {
        super();
        users = new ArrayList<>();
        permissions = new HashSet<>();
    }

    public RoleDto(Long id, String description, String name, UserType userType, List<UserDto> users, Set<PermissionDto> permissions) {
        this();
        this.id = id;
        this.description = description;
        this.name = name;
        this.userType = userType;
        this.users = users;
        this.permissions = permissions;
    }

    public RoleDto(String description, String name, UserType userType, List<UserDto> users, Set<PermissionDto> permissions) {
        this();
        this.description = description;
        this.name = name;
        this.userType = userType;
        this.users = users;
        this.permissions = permissions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

    public Set<PermissionDto> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<PermissionDto> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", userType=" + userType +
                ", users=" + users +
                ", permissions=" + permissions +
                '}';
    }
}
