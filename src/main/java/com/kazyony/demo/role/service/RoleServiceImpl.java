package com.kazyony.demo.role.service;

import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.role.exception.RoleException;
import com.kazyony.demo.role.exception.RoleExceptionMessages;
import com.kazyony.demo.role.mapper.RoleMapper;
import com.kazyony.demo.role.repository.RoleRepository;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.dto.CreateRoleRequestDto;
import com.kazyony.demo.util.PageRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Hassan Wael
 */
@Service
public class RoleServiceImpl implements RoleService {

    private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleMapper mapper;

    /* method service used to find all roles by userType */
    @Override
    public List<RoleDto> findByUserType(UserType userType) {
        logger.info("##-- inside findByUserType --##");
        List<Role> roles = roleRepository.findAllByUserType(userType);
        List<RoleDto> roleDtoList = mapper.convertRoleListToDto(roles);
        return roleDtoList;
    }

    /* method service used to find all roles paginated */
    @Override
    public Page<RoleDto> findAllRolesDtos(Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException {
        List<Role> roles = roleRepository.findAll();
        List<Role> backOfficeRoles = roles.stream().filter(role -> role.getUserType()
                .equals(UserType.BACK_OFFICE)).collect(Collectors.toList());
        Page<Role> backOfficeRolesPage = new PageImpl<>(backOfficeRoles, PageRequestBuilder.build(pageNumber, pageSize, sortField, sortOrder), backOfficeRoles.size());
        return backOfficeRolesPage.map(role -> mapper.convertRoleToDto(role));
    }

    /* method service used to find all roles paginated with filtered values */
    @Override
    public Page<RoleDto> findAllRolesDtos(Specification<Role> specification, Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException {
        Page<Role> roles = roleRepository.findAll(specification, PageRequestBuilder.build(pageNumber, pageSize, sortField, sortOrder));
        List<Role> backOfficeRoles = roles.stream().filter(role -> role.getUserType()
                .equals(UserType.BACK_OFFICE)).collect(Collectors.toList());
        Page<Role> backOfficeRolesPage = new PageImpl<>(backOfficeRoles);
        return backOfficeRolesPage.map(role -> mapper.convertRoleToDto(role));
    }

    /* method service used to find role by id */
    @Override
    public Role findRoleById(Long id) throws RoleException {
        if (id == null) {
            logger.info("##-- role id is null --##");
            throw new RoleException(RoleExceptionMessages.API_NULL_ROLE_ID);
        }
        Role role = roleRepository.findById(id).orElse(null);
        if (role == null) {
            logger.info("##-- role is null --##");
            throw new RoleException(RoleExceptionMessages.API_NULL_ROLE);
        }
        logger.info("##-- role with id {} is found --##", id);
        return role;
    }

    /* method service used to create new role */
    @Override
    public Role createRole(Role role) throws RoleException {
        Role roleInDb = roleRepository.findByName(role.getName());
        if (roleInDb != null) {
            logger.info("##-- role name is found in database (roleName is unique) --##");
            throw new RoleException(RoleExceptionMessages.API_UNIQUE_ROLE_NAME);
        }
        logger.info("##-- new Role is created --##");
        return roleRepository.save(role);
    }

    /* method service used to update role */
    @Override
    public Role updateRole(Role role) throws RoleException {
        Role updatedRole = findRoleById(role.getId());
        if (updatedRole == null) {
            logger.info("##-- role is not found in database --##");
            throw new RoleException(RoleExceptionMessages.API_NOT_FOUND_IN_DB);
        }
        updatedRole.setDescription(role.getDescription());
        updatedRole.setName(role.getName());
        updatedRole.setPermissions(role.getPermissions());
        updatedRole.setUsers(role.getUsers());
        logger.info("##-- Role is updated --##");
        return roleRepository.save(updatedRole);
    }

    /* updating role with response of CreateRoleRequestDto */
    @Override
    public CreateRoleRequestDto updateRole(CreateRoleRequestDto roleDto) throws RoleException {
        Role roleInDb = roleRepository.findByNameAndIdNot(roleDto.getName(), roleDto.getId());
        if (roleInDb != null) {
            logger.info("##-- role name is found in database (roleName is unique) --##");
            throw new RoleException(RoleExceptionMessages.API_UNIQUE_ROLE_NAME);
        }
        Role role = updateRole(mapper.convertRoleToEntity(roleDto));
        if (role == null) {
            throw new RoleException(RoleExceptionMessages.API_PERSISTING_ERROR);
        }
        return mapper.convertCreateRoleToDto(role);
    }

    /* method service used to delete role by id */
    @Override
    public void deleteRole(Long id) throws RoleException {
        Role role = findRoleById(id);
        if (role == null) {
            logger.info("##-- role is not found in database --##");
            throw new RoleException(RoleExceptionMessages.API_NOT_FOUND_IN_DB);
        }
        try {
            roleRepository.delete(role);
            logger.info("##-- role with id {} is deleted --##", id);
        } catch (Exception e) {
            throw new RoleException(RoleExceptionMessages.API_ROLE_DELETE_ERROR);
        }
    }
}
