package com.kazyony.demo.role.service;

import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.role.exception.RoleException;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.dto.CreateRoleRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

/**
 * @author Hassan Wael
 */
public interface RoleService {

    /* method service used to find all roles by userType */
    List<RoleDto> findByUserType(UserType userType);

    /* method service used to find all roles paginated */
    Page<RoleDto> findAllRolesDtos(Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException;

    /* method service used to find all roles paginated with filtered values */
    Page<RoleDto> findAllRolesDtos(Specification<Role> specification, Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException;

    /* method service used to find role by id */
    Role findRoleById(Long id) throws RoleException;

    /* method service used to create new role */
    Role createRole(Role role) throws RoleException;

    /* method service used to update role */
    Role updateRole(Role role) throws RoleException;

    /* updating role with response of CreateRoleRequestDto */
    CreateRoleRequestDto updateRole(CreateRoleRequestDto roleDto) throws RoleException;

    /* method service used to delete role by id */
    void deleteRole(Long id) throws RoleException;
}
