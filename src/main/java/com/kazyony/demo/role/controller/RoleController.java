package com.kazyony.demo.role.controller;

import com.kazyony.demo.core.errorhandling.ErrorResponse;
import com.kazyony.demo.core.errorhandling.LoyaltyBadRequestException;
import com.kazyony.demo.core.jwt.exception.AuthenticationExceptionMessages;
import com.kazyony.demo.core.model.FilterDto;
import com.kazyony.demo.core.specification.SearchSpecification;
import com.kazyony.demo.permission.constants.PermissionsLiterals;
import com.kazyony.demo.permission.dto.PermissionDto;
import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.role.entity.Role_;
import com.kazyony.demo.role.exception.RoleExceptionMessages;
import com.kazyony.demo.role.mapper.RoleMapper;
import com.kazyony.demo.role.service.RoleService;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.dto.CreateRoleRequestDto;
import com.kazyony.demo.util.FiltersParser;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author Hassan Wael
 */
@RestController
@RequestMapping("/v1/users/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private SearchSpecification<Role, Role_> searchSpecification;

    @Autowired
    private RoleMapper roleMapper;

    /* This endpoint is for getting all roles */
    @GetMapping
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.ROLE_ALL_ACTIONS, PermissionsLiterals.ROLE_VIEW_AND_ADD,
            PermissionsLiterals.ROLE_VIEW_AND_UPDATE, PermissionsLiterals.ROLE_VIEW, PermissionsLiterals.ROLE_VIEW_AND_DELETE})
    @ApiOperation(value = "This endpoint is for getting all roles", response = RoleDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class)
    })
    public ResponseEntity<?> getAllRoles(@RequestParam(value = "pageNum", required = false) Integer pageNumber,
                                         @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                         @RequestParam(value = "sortField", required = false) String sortField,
                                         @RequestParam(value = "sortOrder", required = false) Integer sortOrder,
                                         @RequestParam(value = "filters", required = false) String filters) throws IOException {
        Page<RoleDto> roleDtoList;
        if (filters != null && !filters.isEmpty()) {
            List<FilterDto> filterDtoList = FiltersParser.parseFilters(filters);
            Specification<Role> searchSpec = searchSpecification.filter(filterDtoList, Role_.class);
            roleDtoList = roleService.findAllRolesDtos(searchSpec, pageNumber, pageSize, sortField, sortOrder);
        } else {
            roleDtoList = roleService.findAllRolesDtos(pageNumber, pageSize, sortField, sortOrder);
        }
        return new ResponseEntity<>(roleDtoList, HttpStatus.OK);
    }

    /* This endpoint is for getting role by id */
    @GetMapping("/{id}")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.ROLE_ALL_ACTIONS, PermissionsLiterals.ROLE_VIEW_AND_ADD,
            PermissionsLiterals.ROLE_VIEW_AND_UPDATE, PermissionsLiterals.ROLE_VIEW, PermissionsLiterals.ROLE_VIEW_AND_DELETE})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE_ID, response = ErrorResponse.class)
    })
    public ResponseEntity<?> getRoleById(@PathVariable Long id) {
        try {
            Role role = roleService.findRoleById(id);
            RoleDto roleDto = roleMapper.convertRoleToDto(role);
            return new ResponseEntity<>(roleDto, HttpStatus.OK);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /* This endpoint is for getting roles by userType */
    @GetMapping(params = "userType")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.ROLE_ALL_ACTIONS, PermissionsLiterals.ROLE_VIEW_AND_ADD,
            PermissionsLiterals.ROLE_VIEW_AND_UPDATE, PermissionsLiterals.ROLE_VIEW,
            PermissionsLiterals.ROLE_VIEW_AND_DELETE,
            PermissionsLiterals.BACKOFFICE_VIEW_AND_ADD,
            PermissionsLiterals.BACKOFFICE_VIEW_AND_UPDATE,
            PermissionsLiterals.BACKOFFICE_ALL_ACTIONS})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE_ID, response = ErrorResponse.class)
    })
    public ResponseEntity<?> getAllRolesByUserType(@RequestParam UserType userType) {
        List<RoleDto> roleDtoList = roleService.findByUserType(userType);
        return new ResponseEntity<>(roleDtoList, HttpStatus.OK);
    }

    /* This endpoint is to create new Role */
    @PostMapping
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.ROLE_ALL_ACTIONS, PermissionsLiterals.ROLE_VIEW_AND_ADD})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_UNIQUE_ROLE_NAME, response = ErrorResponse.class),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE_ID, response = ErrorResponse.class)
    })
    public ResponseEntity<?> createRole(@Valid @RequestBody RoleDto roleDto) {
        try {
            if (roleDto == null) {
                throw new LoyaltyBadRequestException(RoleExceptionMessages.API_NULL_ROLE);
            }
            Role role = roleMapper.convertRoleToEntity(roleDto);
            Role newRole = roleService.createRole(role);
            RoleDto resultDto = roleMapper.convertRoleToDto(newRole);
            return new ResponseEntity<>(resultDto, HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /* This endpoint is to update Role */
    @PutMapping
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.ROLE_ALL_ACTIONS, PermissionsLiterals.ROLE_VIEW_AND_UPDATE})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE + ", " +
                    RoleExceptionMessages.API_NULL_ROLE_ID + ", " + RoleExceptionMessages.API_NOT_FOUND_IN_DB +
                    ", " + RoleExceptionMessages.API_UNIQUE_ROLE_NAME, response = ErrorResponse.class),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE_ID, response = ErrorResponse.class)
    })
    public ResponseEntity<?> updateRole(@Valid @RequestBody CreateRoleRequestDto roleDto) {
        try {
            if (roleDto == null) {
                throw new LoyaltyBadRequestException(RoleExceptionMessages.API_NULL_ROLE);
            } else if (roleDto.getId() == null) {
                throw new LoyaltyBadRequestException(RoleExceptionMessages.API_NULL_ROLE_ID);
            }
            CreateRoleRequestDto updatedRole = roleService.updateRole(roleDto);
            if (updatedRole == null) {
                throw new LoyaltyBadRequestException(RoleExceptionMessages.API_NOT_FOUND_IN_DB);
            }
            return new ResponseEntity<>(updatedRole, HttpStatus.OK);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /* This endpoint is to delete role */
    @DeleteMapping("/{id}")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.ROLE_ALL_ACTIONS, PermissionsLiterals.ROLE_VIEW_AND_DELETE})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE_ID, response = ErrorResponse.class),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = RoleExceptionMessages.API_NULL_ROLE_ID, response = ErrorResponse.class)
    })
    public ResponseEntity<?> deleteRole(@PathVariable Long id) {
        try {
            if (id == null) {
                throw new LoyaltyBadRequestException(RoleExceptionMessages.API_NULL_ROLE_ID);
            }
            roleService.deleteRole(id);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }
}
