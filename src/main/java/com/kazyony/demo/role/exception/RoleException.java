package com.kazyony.demo.role.exception;

/**
 * @author Hassan Wael
 */
public class RoleException extends Exception {
    public RoleException() {
    }

    public RoleException(String message) {
        super(message);
    }

    public RoleException(String message, Throwable cause) {
        super(message, cause);
    }
}
