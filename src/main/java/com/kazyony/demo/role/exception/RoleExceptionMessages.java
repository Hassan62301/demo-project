package com.kazyony.demo.role.exception;

/**
 * @author Hassan Wael
 */
public class RoleExceptionMessages {

    // role was not found in database
    public static final String API_NOT_FOUND_IN_DB = "API_NOT_FOUND_IN_DB";

    // role object cannot be null
    public static final String API_NULL_ROLE = "API_NULL_ROLE";

    // role description cannot be null
    public static final String API_NULL_ROLE_DESCRIPTION = "API_NULL_ROLE_DESCRIPTION";

    // error happened while persisting database
    public static final String API_PERSISTING_ERROR = "API_PERSISTING_ERROR";

    // role id cannot be null
    public static final String API_NULL_ROLE_ID = "API_NULL_ROLE_ID";

    // role name exist in database
    public static final String API_UNIQUE_ROLE_NAME = "API_UNIQUE_ROLE_NAME";

    // Error Happened while trying to delete Role
    public static final String API_ROLE_DELETE_ERROR = "API_ROLE_DELETE_ERROR";
}
