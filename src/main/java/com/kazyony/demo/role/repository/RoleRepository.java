package com.kazyony.demo.role.repository;

import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.user.constants.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {

    /* find Role by role name */
    Role findByName(String name);

    /* find Role by name and not by its current id (used for updating role) */
    Role findByNameAndIdNot(String name, Long id);

    /* find all roles by their userType */
    List<Role> findAllByUserType(UserType userType);

}
