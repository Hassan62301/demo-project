package com.kazyony.demo.core.specification;

import com.kazyony.demo.core.model.AuditModel;
import com.kazyony.demo.core.model.AuditModel_;
import com.kazyony.demo.core.model.FilterDto;
import org.reflections.Reflections;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class SearchSpecification<T, E> {

    public Specification<T> filter(List<FilterDto> filterDtoList, Class<E> metaModelType) {
        return (root, query, cb) -> {
            Collection<Predicate> predicates = new ArrayList<>();
            String regex = "\\d+(\\.\\d+)?"; // check if string is number, so we dont validate on case insensitive
            filterDtoList.forEach(filter -> {
                Predicate predicate = null;
                List<Predicate> predicateList = new ArrayList<>();
                List<Predicate> columnPredicates = new ArrayList<>();
                String operation = null;
                try {
                    for (int index = 0; index < filter.getValue().size(); index++) {
                        String value = filter.getValue().toArray(new String[filter.getValue().size()])[index];
                        operation = filter.getOperation();
                        String field = filter.getField();
                        if (value != null && !value.isEmpty()) {
                            if (operation.equals("=")) {
                                if (field.equals("lastModifiedBy")) {
                                    predicate = cb.equal(cb.lower(root.get(AuditModel_.lastModifiedBy.getName())), value.toLowerCase());
                                } else if (field.equals("createdBy")) {
                                    predicate = cb.equal(cb.lower(root.get(AuditModel_.createdBy.getName())), value.toLowerCase());
                                } else if (field.equals("lastModifiedDate")) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                    LocalDateTime startDatevalue = LocalDateTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MIN);
                                    LocalDateTime endDatevalue = LocalDateTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MAX);
                                    predicate = cb.between(root.get(AuditModel_.lastModifiedDate.getName()), startDatevalue, endDatevalue);
                                } else if (field.equals("createdDate")) {
                                    /**
                                     * we can search to the exact hour and minute but this would be really bad for the ui experience
                                     DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                     LocalDateTime startDatevalue =  LocalDateTime.parse(value, format);
                                     LocalDateTime endDatevalue = startDatevalue.plusSeconds(59).plusNanos(999000000);
                                     **/
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                    LocalDateTime startDatevalue = LocalDateTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MIN);
                                    LocalDateTime endDatevalue = LocalDateTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MAX);
                                    predicate = cb.between(root.get(AuditModel_.createdDate.getName()), startDatevalue, endDatevalue);
                                } else if (field.contains(".")) {
                                    predicate = getNestedFieldPredicate(field.split("\\.")[0], field.split("\\.")[1], cb, root, metaModelType, value, false, operation);
                                } else if (checkIfFieldIsEntityType(metaModelType, field)) {
                                    predicate = joinFields(field, cb, root, metaModelType, value.toLowerCase(), false, false);
                                } else if (checkIfFieldIsDate(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                    LocalDateTime startDatevalue = LocalDateTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MIN);
                                    LocalDateTime endDatevalue = LocalDateTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MAX);
                                    predicate = cb.between(root.get(metaModelType.getField(field).getName()), startDatevalue, endDatevalue);
                                } else if (checkIfFieldIsEnum(metaModelType, field)) {
                                    Class c = Class.forName(((SingularAttribute) metaModelType.getField(field).get(null)).getType().getJavaType().getName());
                                    Enum<?> enumValue = null;
                                    for (Object obj : c.getEnumConstants()) {
                                        if (((Enum<?>) obj).name().equals(value)) {
                                            enumValue = (Enum<?>) obj;
                                            break;
                                        }
                                    }
                                    predicate = cb.equal(root.get(metaModelType.getField(field).getName()), enumValue);
                                } else if (checkIfFieldIsTime(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                                    LocalTime startTimeValue = LocalTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getHour(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMinute());
                                    predicate = cb.equal(root.get(metaModelType.getField(field).getName()), startTimeValue);
                                } else if (checkIfFieldIsLocalDate(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                    LocalDate startDateValue = LocalDate.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getYear(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMonth(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getDayOfMonth());
                                    predicate = cb.equal(root.get(metaModelType.getField(field).getName()), startDateValue);
                                } else {
                                    if (((SingularAttribute) metaModelType.getField(field).get(null))
                                            .getType().getJavaType().equals(Boolean.TYPE)) {
                                        predicate = cb.equal(root.get(metaModelType.getField(field).getName()), Boolean.parseBoolean(value));
                                    } else {
                                        if (value.matches(regex)) {
                                            predicate = cb.equal(root.get(metaModelType.getField(field).getName()), value);
                                        } else {
                                            predicate = cb.equal(cb.lower(root.get(metaModelType.getField(field).getName())), value.toLowerCase());
                                        }
                                    }
                                }
                            } else if (operation.equals("!=")) {
                                if (field.equals("lastModifiedBy")) {
                                    predicate = cb.notEqual(cb.lower(root.get(AuditModel_.lastModifiedBy.getName())), value.toLowerCase());
                                } else if (field.equals("createdBy")) {
                                    predicate = cb.notEqual(cb.lower(root.get(AuditModel_.createdBy.getName())), value.toLowerCase());
                                } else if (field.contains(".")) {
                                    predicate = getNestedFieldPredicate(field.split("\\.")[0], field.split("\\.")[1], cb, root, metaModelType, value, true, operation);
                                } else if (checkIfFieldIsEntityType(metaModelType, field)) {
                                    predicate = joinFields(field, cb, root, metaModelType, value.toLowerCase(), true, false);
                                } else if (checkIfFieldIsEnum(metaModelType, field)) {
                                    Class c = Class.forName(((SingularAttribute) metaModelType.getField(field).get(null)).getType().getJavaType().getName());
                                    Enum<?> enumValue = null;
                                    for (Object obj : c.getEnumConstants()) {
                                        if (((Enum<?>) obj).name().equals(value)) {
                                            enumValue = (Enum<?>) obj;
                                            break;
                                        }
                                    }
                                    predicate = cb.notEqual(root.get(metaModelType.getField(field).getName()), enumValue);
                                } else if (checkIfFieldIsTime(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                                    LocalTime startTimeValue = LocalTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getHour(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMinute());
                                    predicate = cb.notEqual(root.get(metaModelType.getField(field).getName()), startTimeValue);
                                } else if (checkIfFieldIsLocalDate(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                    LocalDate startDateValue = LocalDate.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getYear(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMonth(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getDayOfMonth());
                                    predicate = cb.notEqual(root.get(metaModelType.getField(field).getName()), startDateValue);
                                } else {
                                    if (((SingularAttribute) metaModelType.getField(field).get(null))
                                            .getType().getJavaType().equals(Boolean.TYPE)) {
                                        predicate = cb.notEqual(root.get(metaModelType.getField(field).getName()), Boolean.parseBoolean(value));
                                    } else {
                                        if (value.matches(regex)) {
                                            predicate = cb.notEqual(root.get(metaModelType.getField(field).getName()), value);
                                        } else {
                                            predicate = cb.notEqual(cb.lower(root.get(metaModelType.getField(field).getName())), value.toLowerCase());
                                        }
                                        predicateList.add(predicate);
                                        predicate = cb.isNull(root.get(metaModelType.getField(field).getName()));
                                        predicateList.add(predicate);
                                        predicate = cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
                                    }
                                }
                            } else if (operation.equals("%")) {
                                value = value.replaceAll("\\*", "%");
                                if (field.contains(".")) {
                                    predicate = getNestedFieldPredicate(field.split("\\.")[0], field.split("\\.")[1], cb, root, metaModelType, value, false, operation);
                                } else if (checkIfFieldIsEntityType(metaModelType, field)) {
                                    predicate = joinFields(field, cb, root, metaModelType, value.toLowerCase(), false, true);
                                } else {
                                    String[] oringValues = value.split("\\|");
                                    for (int i = 0; i < oringValues.length; i++) {
                                        predicate = cb.like(cb.lower(root.get(metaModelType.getField(field).getName())), oringValues[i].toLowerCase());
                                        predicateList.add(predicate);
                                    }
                                    predicate = cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
                                }
                            } else if (operation.equals(">")) {
                                if (field.equals("lastModifiedDate")) {
//                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                                LocalDateTime datevalue = LocalDateTime.of(format.parse(value).toInstant()
//                                        .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MAX);
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime datevalue = LocalDateTime.parse(value, format).plusSeconds(59).plusNanos(999000000);
                                    predicate = cb.greaterThan(root.get(AuditModel_.lastModifiedDate.getName()), datevalue);
                                } else if (field.equals("createdDate")) {
//                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                                LocalDateTime datevalue = LocalDateTime.of(format.parse(value).toInstant()
//                                        .atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MAX);
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime datevalue = LocalDateTime.parse(value, format).plusSeconds(59).plusNanos(999000000);
                                    predicate = cb.greaterThan(root.get(AuditModel_.createdDate.getName()), datevalue);
                                } else if (field.contains(".")) {
                                    predicate = getNestedFieldPredicate(field.split("\\.")[0], field.split("\\.")[1], cb, root, metaModelType, value, false, operation);
                                } else if (checkIfFieldIsDate(metaModelType, field)) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime datevalue = LocalDateTime.parse(value, format).plusSeconds(59).plusNanos(999000000);
                                    predicate = cb.greaterThan(root.get(metaModelType.getField(field).getName()), datevalue);
                                } else if (checkIfFieldIsTime(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                                    LocalTime startTimeValue = LocalTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getHour(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMinute());
                                    predicate = cb.greaterThan(root.get(metaModelType.getField(field).getName()), startTimeValue);
                                } else if (checkIfFieldIsLocalDate(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                    LocalDate startDateValue = LocalDate.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getYear(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMonth(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getDayOfMonth());
                                    predicate = cb.greaterThan(root.get(metaModelType.getField(field).getName()), startDateValue);
                                } else {
                                    predicate = cb.greaterThan(root.get(metaModelType.getField(field).getName()), value);
                                }
                            } else if (operation.equals(">=")) {
                                if (field.equals("lastModifiedDate")) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.greaterThanOrEqualTo(root.get(AuditModel_.lastModifiedDate.getName()), dateValue);
                                } else if (field.equals("createdDate")) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.greaterThanOrEqualTo(root.get(AuditModel_.createdDate.getName()), dateValue);
                                } else if (checkIfFieldIsDate(metaModelType, field)) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.greaterThanOrEqualTo(root.get(metaModelType.getField(field).getName()), dateValue);
                                } else {
                                    predicate = cb.greaterThanOrEqualTo(root.get(metaModelType.getField(field).getName()), value);
                                }
                            } else if (operation.equals("<")) {
                                if (field.equals("lastModifiedDate")) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.lessThan(root.get(AuditModel_.lastModifiedDate.getName()), dateValue);
                                } else if (field.equals("createdDate")) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.lessThan(root.get(AuditModel_.createdDate.getName()), dateValue);
                                } else if (field.contains(".")) {
                                    predicate = getNestedFieldPredicate(field.split("\\.")[0], field.split("\\.")[1], cb, root, metaModelType, value, false, operation);
                                } else if (checkIfFieldIsDate(metaModelType, field)) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.lessThan(root.get(metaModelType.getField(field).getName()), dateValue);
                                } else if (checkIfFieldIsTime(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                                    LocalTime startTimeValue = LocalTime.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getHour(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMinute());
                                    predicate = cb.lessThan(root.get(metaModelType.getField(field).getName()), startTimeValue);
                                } else if (checkIfFieldIsLocalDate(metaModelType, field)) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                    LocalDate startDateValue = LocalDate.of(format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getYear(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getMonth(), format.parse(value).toInstant()
                                            .atZone(ZoneId.systemDefault()).getDayOfMonth());
                                    predicate = cb.lessThan(root.get(metaModelType.getField(field).getName()), startDateValue);
                                } else {
                                    predicate = cb.lessThan(root.get(metaModelType.getField(field).getName()), value);
                                }
                            } else if (operation.equals("<=")) {
                                if (field.equals("lastModifiedDate")) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.lessThanOrEqualTo(root.get(AuditModel_.lastModifiedDate.getName()), dateValue);
                                } else if (field.equals("createdDate")) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.lessThanOrEqualTo(root.get(AuditModel_.createdDate.getName()), dateValue);
                                } else if (checkIfFieldIsDate(metaModelType, field)) {
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                                    LocalDateTime dateValue = LocalDateTime.parse(value, format);
                                    predicate = cb.lessThanOrEqualTo(root.get(metaModelType.getField(field).getName()), dateValue);
                                } else {
                                    predicate = cb.lessThanOrEqualTo(root.get(metaModelType.getField(field).getName()), value);
                                }
                            }
                        }
                        columnPredicates.add(predicate);
                    }
                    if (operation.equals("!=")) {
                        predicates.add(cb.and(columnPredicates.toArray(new Predicate[columnPredicates.size()])));
                    } else {
                        predicates.add(cb.or(columnPredicates.toArray(new Predicate[columnPredicates.size()])));
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            });

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    private boolean checkIfFieldIsEntityType(Class<E> metaModelType, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        //maybe try w catch block and repeat in case of nosuchelement
        if (fieldName.equals("createdBy") || fieldName.equals("lastModifiedBy")) {
            if (((SingularAttribute) metaModelType.getSuperclass().getDeclaredField(fieldName).get(null)).getType().getJavaType()
                    .getSuperclass() == null) {
                return false;
            }
            return ((SingularAttribute) metaModelType.getSuperclass().getDeclaredField(fieldName).get(null)).getType().getJavaType()
                    .getSuperclass().equals(AuditModel.class);
        } else {
            if (((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                    .getSuperclass() == null) {
                return false;
            }
            return ((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                    .getSuperclass().equals(AuditModel.class);
        }
    }

    private boolean checkIfFieldIsDate(Class<E> metaModelType, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        return ((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                .equals(LocalDateTime.class);
    }

    private boolean checkIfFieldIsEnum(Class<E> metaModelType, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        if (((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                .getSuperclass() == null) {
            return false;
        }
        return ((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                .getSuperclass().equals(Enum.class);
    }

    private Class<?> checkMetaModelType(Class<E> metaModelType, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        return ((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType();
    }

    private boolean checkIfFieldIsTime(Class<E> metaModelType, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        return ((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                .equals(LocalTime.class);
    }

    private boolean checkIfFieldIsLocalDate(Class<E> metaModelType, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        return ((SingularAttribute) metaModelType.getField(fieldName).get(null)).getType().getJavaType()
                .equals(LocalDate.class);
    }

    private Predicate getNestedFieldPredicate(String fieldName, String nestedFieldName, CriteriaBuilder cb,
                                              Root<?> root, Class<E> metaModelType, String value, boolean negate, String operation) throws NoSuchFieldException, IllegalAccessException {
        Reflections reflections = new Reflections("com.bs");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(StaticMetamodel.class);
        Iterator<Class<?>> it = annotated.iterator();
        String regex = "\\d+(\\.\\d+)?";
        while (it.hasNext()) {
            Class<?> joinedMetaModel = it.next();
            if (joinedMetaModel.getAnnotation(StaticMetamodel.class).value().equals(checkMetaModelType(metaModelType, fieldName))) {
                if (negate) {
                    if (!operation.equals("%")) {
                        if (value.matches(regex)) {
                            return cb.notEqual(root.get(metaModelType.getField(fieldName).getName())
                                    .get(joinedMetaModel.getField(nestedFieldName).getName()), value.toString());
                        }
                        return cb.notEqual(cb.lower(root.get(metaModelType.getField(fieldName).getName())
                                .get(joinedMetaModel.getField(nestedFieldName).getName())), value.toLowerCase());
                    }
                    return cb.notLike(cb.lower(root.get(metaModelType.getField(fieldName).getName())
                            .get(joinedMetaModel.getField(nestedFieldName).getName())), value.toString());
                } else if (operation.equals(">")) {
                    if (value.matches(regex)) {
                        return cb.greaterThan(root.get(metaModelType.getField(fieldName).getName())
                                .get(joinedMetaModel.getField(nestedFieldName).getName()), value);
                    }
                } else if (operation.equals("<")) {
                    if (value.matches(regex)) {
                        return cb.lessThan(root.get(metaModelType.getField(fieldName).getName())
                                .get(joinedMetaModel.getField(nestedFieldName).getName()), value);
                    }
                } else {
                    if (!operation.equals("%")) {
                        if (value.matches(regex)) {
                            return cb.equal(root.get(metaModelType.getField(fieldName).getName())
                                    .get(joinedMetaModel.getField(nestedFieldName).getName()), value.toString());
                        }
                        return cb.equal(cb.lower(root.get(metaModelType.getField(fieldName).getName())
                                .get(joinedMetaModel.getField(nestedFieldName).getName())), value.toLowerCase());
                    }
                    return cb.like(cb.lower(root.get(metaModelType.getField(fieldName).getName())
                            .get(joinedMetaModel.getField(nestedFieldName).getName())), value.toLowerCase());
                }
            }
        }
        return null;
    }

    private Predicate joinFields(String fieldName, CriteriaBuilder cb, Root<?> root, Class<E> metaModelType
            , Object value, boolean negate, boolean like) throws NoSuchFieldException, IllegalAccessException {
        Reflections reflections = new Reflections("com.bs");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(StaticMetamodel.class);
        Iterator<Class<?>> it = annotated.iterator();
        while (it.hasNext()) {
            Class<?> joinedMetaModel = it.next();
            if (joinedMetaModel.getAnnotation(StaticMetamodel.class).value().equals(checkMetaModelType(metaModelType, fieldName))) {
                if (negate) {
                    return cb.notEqual(cb.lower(root.join(metaModelType.getField(fieldName).getName())
                            .get(joinedMetaModel.getField("name").getName())), value.toString());
                } else if (like) {
                    return cb.like(cb.lower(root.join(metaModelType.getField(fieldName).getName())
                            .get(joinedMetaModel.getField("name").getName())), value.toString());
                } else {
                    return cb.equal(cb.lower(root.join(metaModelType.getField(fieldName).getName())
                            .get(joinedMetaModel.getField("name").getName())), value.toString());
                }
            }
        }
        return null;
    }
}
