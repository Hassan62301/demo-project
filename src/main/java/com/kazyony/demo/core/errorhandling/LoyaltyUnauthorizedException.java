package com.kazyony.demo.core.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hassan Wael.
 *
 * Class to represent UNAUTHORIZED Request Excpetions
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class LoyaltyUnauthorizedException extends RuntimeException {

    public LoyaltyUnauthorizedException() {
    }

    public LoyaltyUnauthorizedException(String message) {
        super(message);
    }

    public LoyaltyUnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}

