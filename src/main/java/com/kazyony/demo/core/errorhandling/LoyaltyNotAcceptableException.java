package com.kazyony.demo.core.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hassan Wael.
 *
 * Class to represent NOT_ACCEPTABLE Request Excpetions
 */
@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class LoyaltyNotAcceptableException extends RuntimeException {

    public LoyaltyNotAcceptableException() {
    }

    public LoyaltyNotAcceptableException(String message) {
        super(message);
    }

    public LoyaltyNotAcceptableException(String message, Throwable cause) {
        super(message, cause);
    }
}
