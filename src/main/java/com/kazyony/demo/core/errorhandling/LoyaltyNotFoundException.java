package com.kazyony.demo.core.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hassan Wael.
 *
 * Class to represent NOT_FOUND Request Excpetions
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class LoyaltyNotFoundException extends RuntimeException {

    public LoyaltyNotFoundException() {
    }

    public LoyaltyNotFoundException(String message) {
        super(message);
    }

    public LoyaltyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
