package com.kazyony.demo.core.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hassan Wael.
 *
 * Class to represent CONFLICT Request Excpetions
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class LoyaltyConflictException extends RuntimeException {

    public LoyaltyConflictException(String message) {
        super(message);
    }

    public LoyaltyConflictException(String message, Throwable cause) {
        super(message, cause);
    }
}
