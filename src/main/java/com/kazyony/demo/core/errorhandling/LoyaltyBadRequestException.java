package com.kazyony.demo.core.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hassan Wael.
 *
 * Class to represent BAD_REQUEST Request Excpetions
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LoyaltyBadRequestException extends RuntimeException {

    public LoyaltyBadRequestException() {
    }

    public LoyaltyBadRequestException(String message) {
        super(message);
    }

    public LoyaltyBadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
