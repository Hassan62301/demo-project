package com.kazyony.demo.core.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Hassan Wael.
 *
 * Class to represent FORBIDDEN Request Excpetions
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class LoyaltyForbiddenException extends RuntimeException {

    public LoyaltyForbiddenException(String message) {
        super(message);
    }

    public LoyaltyForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }
}
