package com.kazyony.demo.core.errorhandling;

import java.util.Date;

/**
 * @author Hassan Wael.
 *
 * Class to represent the response data model in case of endpoint errors. (BasicErrorController)
 * Used only for swagger documentation
 *
 */
public class ErrorResponse {

    private Date timestamp;
    private int status;
    private String error;
    private String message;
    private String exception;
    private String trace;
    private String path;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
