package com.kazyony.demo.core.jwt.tokenvalidation;

import com.kazyony.demo.core.jwt.exception.AuthenticationExceptionMessages;
import com.kazyony.demo.core.jwt.tokengeneration.service.CustomUserDetailsService;
import io.jsonwebtoken.lang.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Hassan Wael.
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    @Autowired
    private JwtTokenInterpreter jwtTokenInterpreter;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Value("${loyalty.angular.client.url}")
    private String angularClientUrl;

    /**
     * This method extracts the JWT token from the request and validates if the user is authorized or not
     *
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        logger.info("--## doFilterInternal >> Filtering Request ##--");
        try {
            addCORSHeaders(response);

            String jwt = getJwtFromRequest(request);
            boolean isTokenValid = jwtTokenInterpreter.validateToken(jwt, request, "accessToken");
            boolean isAccessToken = jwtTokenInterpreter.isAccessToken(jwt);
            if (isAccessToken && isTokenValid) {
                logger.info("--## doFilterInternal << found a valid access token in request ##--");
                String username = jwtTokenInterpreter.getUsernameFromJWT(jwt);
                UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
                List<SimpleGrantedAuthority> authorities = extractRolesFromToken(jwt);
                fillSecurityContextWithAuthenticationDetails(userDetails, authorities, request);
            } else {
                logger.info("--## doFilterInternal << no valid access token in request ##--");
            }
        } catch (Exception ex) {
            logger.error("--## doFilterInternal << Cannot authorize this user --## - {}", ex.getMessage());
        }

        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private void addCORSHeaders(HttpServletResponse response) {
        response.addHeader("Access-control-Allow-Origin", angularClientUrl);
        response.addHeader("Access-control-Allow-Methods", "POST , GET , OPTIONS , PUT , DELETE, PATCH");
        response.addHeader("Access-control-Allow-Headers", "Authorization , Origin , content-type");
        response.addHeader("Access-control-Allow-Credentials", "true");
        response.addHeader("Access-control-Max-Age", "1728000");
        response.addHeader("Access-Control-Expose-Headers", "accessToken, refreshToken");
    }

    /**
     * This method fills the security context with authentication details (user details, and its roles)
     *
     * @param userDetails
     * @param authorities
     * @param request
     */
    private void fillSecurityContextWithAuthenticationDetails(UserDetails userDetails, List<SimpleGrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        if (!userDetails.isEnabled()) {
            authentication.setAuthenticated(false);
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    /**
     * This method extracts roles from the JWT token and returns a list of SimpleGrantedAuthorities
     *
     * @param jwt
     * @return
     */
    public List<SimpleGrantedAuthority> extractRolesFromToken(String jwt) {
        logger.info("--## extractRolesFromToken >> extracting roles from token ##--");
        String[] roles = jwtTokenInterpreter.getUserRolesFromJWT(jwt).orElseThrow(() ->
                new AuthorizationServiceException(AuthenticationExceptionMessages.API_EXPIRED_REFRESH_TOKEN)
        );
        List<String> rolesList = Collections.arrayToList(roles);
        List<SimpleGrantedAuthority> authorities = (rolesList.stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList()));
        logger.info("--## extractRolesFromToken << found the following roles: " + authorities.toString() + " ##--");
        return authorities;

    }

    /**
     * This method extracts the jwt token from the request
     *
     * @param request
     * @return
     */
    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
