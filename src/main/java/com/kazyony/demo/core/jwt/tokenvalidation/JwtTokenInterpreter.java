package com.kazyony.demo.core.jwt.tokenvalidation;

import com.kazyony.demo.core.jwt.config.WebSecuritySettings;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * This class's aim is to validate JWT tokens and extract data from it.
 * <p>
 * @author Hassan Wael.
 */
@Service
public class JwtTokenInterpreter {
    public static final String ROLES_CLAIM_KEY = "ROLES";
    public static final String REFRESH_CLAIM_KEY = "REFRESH";
    public static final String REFRESH_CLAIM_VALUE = "ROLE_REFRESH";
    public static final String DELIMITER = ", ";
    public static final String EXPIRED_JWT = "expiredJWT";
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenInterpreter.class);
    @Autowired
    WebSecuritySettings baseSecuritySettings;

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    /**
     * This method extracts user Id from JWT token
     *
     * @param token
     * @return
     */
    public String getUsernameFromJWT(String token) {
        logger.info("--## getUserIdFromJWT: extract User id from Jwt Token ##--");
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }

    /**
     * This method extract user Roles from JWT token
     *
     * @param token
     * @return
     */
    public Optional<String[]> getUserRolesFromJWT(String token) {
        logger.info("--## getUserRolesFromJWT >> extract UserRoles from Token ##--");
        Claims claims = extractClaimsFromJWT(token);
        if (claims.containsKey(ROLES_CLAIM_KEY)) {
            Optional<String[]> roles = Optional.of(((String) claims.get(ROLES_CLAIM_KEY)).split(DELIMITER));
            logger.info("--## getUserRolesFromJWT << found Roles Claim inside Token ##--");
            return roles;
        }
        logger.info("--## getUserRolesFromJWT << No Roles Claim was found inside Token! ##--");
        return Optional.ofNullable(null);
    }

    /**
     * This method extract user Roles from JWT token
     *
     * @param token
     * @return
     */
    public String getRefreshAuthorityFromJWT(String token) {
        logger.info("--## getRefreshAuthorityFromJWT >> extract Refresh Claims from Jwt Token ##--");
        Claims claims = extractClaimsFromJWT(token);
        if (claims.containsKey(REFRESH_CLAIM_KEY)) {
            logger.info("--## getRefreshAuthorityFromJWT << found a refresh claim inside Jwt Token! ##--");
            return (String) claims.get(REFRESH_CLAIM_KEY);
        }
        logger.info("--## getRefreshAuthorityFromJWT << No refresh claim was found inside Jwt Token! ##--");
        return null;
    }

    public boolean isAccessToken(String token) {
        logger.info("--## isAccessToken >> check if token is an access Token ##--");
        if (token != null) {
            try {
                Claims claims = extractClaimsFromJWT(token);
                if (claims.containsKey(ROLES_CLAIM_KEY)) {
                    logger.info("--## isAccessToken << valid Access Token! ##--");
                    return true;
                }
            } catch (ExpiredJwtException ex) {
                logger.error("--## isAccessToken << Expired JWT token ##--");
            }
        }
        logger.info("--## isAccessToken << Not an Access Token! ##--");
        return false;
    }

    public Claims extractClaimsFromJWT(String token) {
        logger.info("--## extractClaimsFromJWT: extracting claims from Jwt Token ##--");
        Claims claims = Jwts.parser().setSigningKey(baseSecuritySettings.getJwtSecret()).parseClaimsJws(token).getBody();
        return claims;
    }

    public boolean validateRefreshToken(String authToken, HttpServletRequest request) {
        logger.info("--## validateRefreshToken >> Validating Refresh Token ##--");
        boolean valid = validateToken(authToken, request, "refreshToken");
        if (valid) {
            String refreshAuthority = getRefreshAuthorityFromJWT(authToken);
            if (refreshAuthority != null && refreshAuthority.equals(REFRESH_CLAIM_VALUE)) {
                logger.info("--## validateRefreshToken << Valid Refresh Token! ##--");
                return valid;
            }
        }

        logger.info("--## validateRefreshToken << Invalid Refresh Token ##--");
        return false;
    }

    /**
     * This method validates JWT tokens by decrypting it against the jwtSecret
     *
     * @param authToken
     * @return
     */
    public boolean validateToken(String authToken, HttpServletRequest request, String tokenType) {
        logger.info("--## validateToken >> Validating JWT Token ##--");
        try {
            if (StringUtils.hasText(authToken)) {
                Jwts.parser().setSigningKey(baseSecuritySettings.getJwtSecret()).parseClaimsJws(authToken);
                logger.info("--## validateToken << Valid JWT Token! ##--");
                return true;
            }
        } catch (SignatureException ex) {
            logger.error("--## validateToken << Invalid JWT signature ##--");
        } catch (MalformedJwtException ex) {
            logger.error("--## validateToken << Invalid JWT token ##--");
        } catch (ExpiredJwtException ex) {
            request.setAttribute(EXPIRED_JWT, tokenType);
            logger.error("--## validateToken << Expired JWT token ##--");
        } catch (UnsupportedJwtException ex) {
            logger.error("--## validateToken << Unsupported JWT token ##--");
        } catch (IllegalArgumentException ex) {
            logger.error("--## validateToken << JWT claims string is empty. ##--");
        }
        return false;
    }
}
