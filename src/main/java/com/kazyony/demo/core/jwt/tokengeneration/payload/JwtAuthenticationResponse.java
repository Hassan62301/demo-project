package com.kazyony.demo.core.jwt.tokengeneration.payload;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Hassan Wael.
 */
public class JwtAuthenticationResponse {

    @ApiModelProperty(notes = "This is the access token")
    private String accessToken;

    @ApiModelProperty(notes = "This is the refresh token")
    private String refreshToken;

    public JwtAuthenticationResponse(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return "JwtAuthenticationResponse [accessToken=####, refreshToken=####]";
    }
}
