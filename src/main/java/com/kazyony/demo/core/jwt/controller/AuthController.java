package com.kazyony.demo.core.jwt.controller;

import com.kazyony.demo.core.errorhandling.ErrorResponse;
import com.kazyony.demo.core.errorhandling.LoyaltyBadRequestException;
import com.kazyony.demo.core.errorhandling.LoyaltyForbiddenException;
import com.kazyony.demo.core.errorhandling.LoyaltyUnauthorizedException;
import com.kazyony.demo.core.jwt.exception.AuthenticationExceptionMessages;
import com.kazyony.demo.core.jwt.tokengeneration.model.JwtTokenFactory;
import com.kazyony.demo.core.jwt.tokengeneration.payload.JwtAuthenticationResponse;
import com.kazyony.demo.core.jwt.tokengeneration.payload.LoginRequest;
import com.kazyony.demo.core.jwt.tokengeneration.payload.NewPasswordRequest;
import com.kazyony.demo.core.jwt.tokengeneration.payload.RefreshTokenRequest;
import com.kazyony.demo.core.jwt.tokengeneration.service.CustomUserDetailsService;
import com.kazyony.demo.core.jwt.tokenvalidation.JwtTokenInterpreter;
import com.kazyony.demo.core.jwt.tokenvalidation.model.UserPrincipal;
import com.kazyony.demo.user.dto.ApiUserDto;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.user.payload.UserDataResponse;
import com.kazyony.demo.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalTime;

/**
 * @author Hassan Wael.
 */
@RestController
@RequestMapping("/api/auth")
@Api(tags = {"Login API"}, value = "Login API")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenFactory tokenFactory;

    @Autowired
    private JwtTokenInterpreter tokenInterpreter;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private UserService userService;


    /**
     * This endpoint serves as a login entry point for outsiders to request jwt tokens and return all needed data to
     * any integrator
     *
     * @param loginRequest LoginRequest
     * @return UserDataResponse
     */
    @PostMapping("/login")
    @ApiOperation(value = "This endpoint is for authenticating users and generating JWT tokens",
            response = UserDataResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = AuthenticationExceptionMessages.API_BAD_CREDENTIAL, response =
                    ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_LOGIN_NOT_ALLOWED
                    + ", " + AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response =
                    ErrorResponse.class)
    })
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        String password = loginRequest.getPassword();

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUserName(), password)
        );
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        try {
            User user = userService.findByUserName(loginRequest.getUserName());

            HttpHeaders responseHeaders = new HttpHeaders();

            if (!user.isInitialPassword()) {

                SecurityContextHolder.getContext().setAuthentication(authentication);

                String accessToken = tokenFactory.generateAccessToken(userPrincipal);
                String refreshToken = tokenFactory.generateRefreshToken(userPrincipal);

                responseHeaders.set("accessToken", accessToken);
                responseHeaders.set("refreshToken", refreshToken);

                return ResponseEntity.ok().headers(responseHeaders).body(userService.getUserDataResponse(user));
            }
            ApiUserDto apiUserDto = userService.getApiUserDtoFromUser(user);
            return ResponseEntity.ok().headers(responseHeaders).body(new UserDataResponse(apiUserDto,
                    LocalTime.now()));
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /**
     * This endpoint is setting new password for users and return all needed data for any integrator
     *
     * @param newPasswordRequest NewPasswordRequest
     * @return UserDataResponse
     */
    @PostMapping("/set-password")
    @ApiOperation(value = "This endpoint is for setting password of users for the first time", response =
            UserDataResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = AuthenticationExceptionMessages.API_PASSWORD_ALREADY_SET, response =
                    ErrorResponse.class),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response =
                    ErrorResponse.class)
    })
    public ResponseEntity<?> setPassword(@Valid @RequestBody NewPasswordRequest newPasswordRequest) {

        String password = newPasswordRequest.getPassword();
        String newPassword = newPasswordRequest.getNewPassword();

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(newPasswordRequest.getUserName(), password)
        );
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        try {
            User user = userService.findByUserName(newPasswordRequest.getUserName());

            HttpHeaders responseHeaders = new HttpHeaders();

            if (user.isInitialPassword()) {
                user = userService.setNewPassword(user, newPassword);

                SecurityContextHolder.getContext().setAuthentication(authentication);

                String accessToken = tokenFactory.generateAccessToken(userPrincipal);
                String refreshToken = tokenFactory.generateRefreshToken(userPrincipal);

                responseHeaders.set("accessToken", accessToken);
                responseHeaders.set("refreshToken", refreshToken);
            } else {
                throw new LoyaltyBadRequestException(AuthenticationExceptionMessages.API_PASSWORD_ALREADY_SET);
            }
            return ResponseEntity.ok().headers(responseHeaders).body(userService.getUserDataResponse(user));
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /**
     * This endpoint receives a request with a refresh token and returns a response with a new access token
     *
     * @param request             HttpServletRequest
     * @param refreshTokenRequest RefreshTokenRequest
     * @return JwtAuthenticationResponse
     */
    @PostMapping("/refresh-token")
    @ApiOperation(value = "This endpoint is for authenticating users and generating JWT tokens", response =
            JwtAuthenticationResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = JwtAuthenticationResponse.class, message = "OK"),
            @ApiResponse(code = 400, message = AuthenticationExceptionMessages.API_INVALID_JWT, response =
                    ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_EXPIRED_REFRESH_TOKEN, response =
                    ErrorResponse.class),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response =
                    ErrorResponse.class)
    })
    public ResponseEntity<?> refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest,
                                          HttpServletRequest request) {
        boolean isTokenValid = tokenInterpreter.validateRefreshToken(refreshTokenRequest.getRefreshToken(), request);
        Object expiredJWT = request.getAttribute(JwtTokenInterpreter.EXPIRED_JWT);
        if ("refreshToken".equals(expiredJWT)) {
            throw new LoyaltyUnauthorizedException(AuthenticationExceptionMessages.API_EXPIRED_REFRESH_TOKEN);
        } else if (isTokenValid) {
            String userName = tokenInterpreter.getUsernameFromJWT(refreshTokenRequest.getRefreshToken());
            UserPrincipal userPrincipal = (UserPrincipal) customUserDetailsService.loadUserByUsername(userName);
            if (userPrincipal.isEnabled()) {
                String accessToken = tokenFactory.generateAccessToken(userPrincipal);
                String refreshToken = tokenFactory.generateRefreshToken(userPrincipal);

                return ResponseEntity.ok(new JwtAuthenticationResponse(accessToken, refreshToken));
            } else {
                throw new LoyaltyForbiddenException(AuthenticationExceptionMessages.API_INACTIVE_USER);
            }
        } else {
            throw new LoyaltyBadRequestException(AuthenticationExceptionMessages.API_INVALID_JWT);
        }
    }

}
