package com.kazyony.demo.core.jwt.tokengeneration.payload;

import javax.validation.constraints.NotBlank;

/**
 * @author Hassan Wael.
 */
public class NewPasswordRequest extends LoginRequest {

    @NotBlank
    private String newPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "NewPasswordRequest{ newPassword=#### }";
    }
}
