package com.kazyony.demo.core.jwt.tokengeneration.model;

import com.kazyony.demo.core.jwt.tokenvalidation.model.UserPrincipal;
import com.kazyony.demo.user.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Customized class for UserDetails; used by Spring Security for validation reasons.
 * <p>
 * @author Hassan Wael.
 */
public class UserToUserPrincipalMapper {

    public static UserPrincipal create(User user) {
        List<GrantedAuthority> authorities = user.getRole().getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getName()))
                .collect(Collectors.toList());

        return new UserPrincipal(user.getUserName(), user.getPassword(), user.isEnabled(), authorities);
    }
}
