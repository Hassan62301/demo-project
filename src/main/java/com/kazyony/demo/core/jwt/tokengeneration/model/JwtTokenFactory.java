package com.kazyony.demo.core.jwt.tokengeneration.model;

import com.kazyony.demo.core.jwt.config.WebSecuritySettings;
import com.kazyony.demo.core.jwt.tokenvalidation.JwtTokenInterpreter;
import com.kazyony.demo.core.jwt.tokenvalidation.model.UserPrincipal;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author Hassan Wael.
 */
@Service
public class JwtTokenFactory {

    @Autowired
    WebSecuritySettings securitySettings;

    /**
     * This method generates access tokens using the user principal
     *
     * @param userPrincipal
     * @return String JWT Token
     */
    public String generateAccessToken(UserPrincipal userPrincipal) {
        HashMap<String, Object> claims = buildClaimOfAuthoritiesFromRoles(userPrincipal);
        String jwtToken = buildJwtAccessToken(claims, userPrincipal);
        return jwtToken;
    }

    /**
     * This method generates refresh tokens using the user principal
     *
     * @param userPrincipal
     * @return
     */
    public String generateRefreshToken(UserPrincipal userPrincipal) {
        HashMap<String, Object> claims = buildClaimsForRefreshToken();
        String jwtToken = buildJwtRefreshToken(claims, userPrincipal);
        return jwtToken;
    }

    /**
     * This method builds JWT access token using a list of claims, and user principal's username as subject,
     * expiry date, jwt secret
     *
     * @param claims
     * @param userPrincipal
     * @return
     */
    private String buildJwtAccessToken(HashMap<String, Object> claims, UserPrincipal userPrincipal) {
        LocalDateTime currentTime = LocalDateTime.now();
        Date expiryTime = Date.from(currentTime
                .plusMinutes(securitySettings.getJwtAccessTokenExpirationInM())
                .atZone(ZoneId.systemDefault()).toInstant());

        String jwtToken = Jwts.builder()
                .setClaims(claims)
                .setSubject(userPrincipal.getUsername() + "")
                .setIssuedAt(new Date())
                .setExpiration(expiryTime)
                .signWith(SignatureAlgorithm.HS512, securitySettings.getJwtSecret())
                .compact();
        return jwtToken;
    }

    /**
     * This method builds JWT refresh token using a list of claims, and user principal's username as subject,
     * expiry date, jwt secret
     *
     * @param claims
     * @param userPrincipal
     * @return
     */
    private String buildJwtRefreshToken(HashMap<String, Object> claims, UserPrincipal userPrincipal) {
        LocalDateTime currentTime = LocalDateTime.now();
        Date expiryTime = Date.from(currentTime
                .plusMinutes(securitySettings.getJwtRefreshTokenExpirationInM())
                .atZone(ZoneId.systemDefault()).toInstant());

        String jwtToken = Jwts.builder()
                .setClaims(claims)
                .setSubject(userPrincipal.getUsername() + "")
                .setIssuedAt(new Date())
                .setId(UUID.randomUUID().toString())
                .setExpiration(expiryTime)
                .signWith(SignatureAlgorithm.HS512, securitySettings.getJwtSecret())
                .compact();

        return jwtToken;
    }

    /**
     * This method builds a claim of authorities taking the roles from the user Principal
     *
     * @param userPrincipal
     * @return
     */
    private HashMap<String, Object> buildClaimOfAuthoritiesFromRoles(UserPrincipal userPrincipal) {
        String authoritiesAsString = userPrincipal.getAuthorities().toString();
        String authoritiesNames = authoritiesAsString.substring(1, authoritiesAsString.length() - 1);
        HashMap<String, Object> claims = new HashMap<>();
        claims.put(JwtTokenInterpreter.ROLES_CLAIM_KEY, authoritiesNames);
        return claims;
    }

    /**
     * This method builds the claims part of a refresh token
     *
     * @return
     */
    private HashMap<String, Object> buildClaimsForRefreshToken() {
        HashMap<String, Object> claims = new HashMap<>();
        claims.put(JwtTokenInterpreter.REFRESH_CLAIM_KEY, JwtTokenInterpreter.REFRESH_CLAIM_VALUE);
        return claims;
    }
}
