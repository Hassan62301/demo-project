package com.kazyony.demo.core.jwt.exception;

/**
 * @author Hassan Wael.
 */
public class AuthenticationExceptionMessages {

    // Cannot Authenticate the user, User is Inactive!
    public static final String API_INACTIVE_USER = "API_INACTIVE_USER";

    // Bad Credential, Wrong Username or Password!
    public static final String API_BAD_CREDENTIAL = "API_BAD_CREDENTIAL";

    // You are not authorized to login
    public static final String API_LOGIN_NOT_ALLOWED = "API_LOGIN_NOT_ALLOWED";

    // Sorry, You're not authorized to access this resource!
    public static final String API_ACCESS_NOT_ALLOWED = "API_ACCESS_NOT_ALLOWED";

    // You have already set your password previously
    public static final String API_PASSWORD_ALREADY_SET = "API_PASSWORD_ALREADY_SET";

    // if the front-end receives this msg they should hit the end-point : /refresh-token
    //don't forget to put the refresh token you got the first time instead of the expired access-token in the Authorization/Bearer
    public static final String API_EXPIRED_ACCESS_TOKEN = "API_EXPIRED_ACCESS_TOKEN";

    // Cannot access resource with refresh token, please refresh your access token first
    public static final String API_EXPIRED_REFRESH_TOKEN = "API_EXPIRED_REFRESH_TOKEN";

    // Invalid JWT Token found in request header
    public static final String API_INVALID_JWT = "API_INVALID_JWT";

    // User is not authorized to access this resource api, you have insufficient permissions
    public static final String API_DENIED_ACCESS = "API_DENIED_ACCESS";
}
