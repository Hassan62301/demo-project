package com.kazyony.demo.core.jwt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Hassan Wael.
 */
@Component
public class WebSecuritySettings {

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtAccessTokenExpirationInMinutes}")
    private int jwtAccessTokenExpirationInM;

    @Value("${app.jwtRefreshTokenExpirationInMinutes}")
    private int jwtRefreshTokenExpirationInM;

    @Value("${app.bcrypt.strength}")
    private int strength;

    public String getJwtSecret() {
        return jwtSecret;
    }

    public void setJwtSecret(String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    public int getJwtAccessTokenExpirationInM() {
        return jwtAccessTokenExpirationInM;
    }

    public void setJwtAccessTokenExpirationInM(int jwtAccessTokenExpirationInM) {
        this.jwtAccessTokenExpirationInM = jwtAccessTokenExpirationInM;
    }

    public int getJwtRefreshTokenExpirationInM() {
        return jwtRefreshTokenExpirationInM;
    }

    public void setJwtRefreshTokenExpirationInM(int jwtRefreshTokenExpirationInM) {
        this.jwtRefreshTokenExpirationInM = jwtRefreshTokenExpirationInM;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }
}
