package com.kazyony.demo.core.jwt.config;

import com.kazyony.demo.core.jwt.exception.AuthenticationExceptionMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Hassan Wael.
 */
public class LoyaltyAccessDeniedHandler implements AccessDeniedHandler {

    private static final Logger logger = LoggerFactory.getLogger(LoyaltyAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) throws IOException, ServletException {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.warn("--## User: " + username + " attempted to access the protected URL: " + request.getRequestURI());
        response.sendError(HttpServletResponse.SC_FORBIDDEN, AuthenticationExceptionMessages.API_DENIED_ACCESS);
    }
}
