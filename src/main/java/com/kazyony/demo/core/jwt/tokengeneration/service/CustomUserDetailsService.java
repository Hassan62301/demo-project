package com.kazyony.demo.core.jwt.tokengeneration.service;

import com.kazyony.demo.core.errorhandling.LoyaltyBadRequestException;
import com.kazyony.demo.core.jwt.tokengeneration.model.UserToUserPrincipalMapper;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.user.exception.UserExceptionMessages;
import com.kazyony.demo.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Hassan Wael.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
    @Autowired
    UserRepository userRepository;

    /**
     * This method finds users by username for the sake of authentication
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> users = userRepository.findAllByUserName(username);
        if (users == null || users.isEmpty()) {
            logger.info("--## CustomUserDetailService <- cannot find user by username " + username + " ##--");
            throw new UsernameNotFoundException(UserExceptionMessages.API_NOT_FOUND_USER_BY_USER_NAME);
        }
        if (users.size() > 1) {
            logger.info("--## CustomUserDetailService <- Dublicate username is not allowed" + username + " ##--");
            throw new LoyaltyBadRequestException(UserExceptionMessages.API_DUPLICATE_USERNAME);
        }
        UserDetails userDetails = UserToUserPrincipalMapper.create(users.get(0));
        return userDetails;
    }
}
