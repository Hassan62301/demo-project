package com.kazyony.demo.core.jwt.config;

import com.kazyony.demo.core.jwt.tokengeneration.service.CustomUserDetailsService;
import com.kazyony.demo.core.jwt.tokenvalidation.JwtAuthenticationEntryPoint;
import com.kazyony.demo.core.jwt.tokenvalidation.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This class configures our application by pointing to the
 * protected resources and the non-protected ones, and configuring a filter to intercept requests
 * <p>
 *
 * @author Hassan Wael.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String AUTHENTICATION_HEADER_NAME = "Authorization";

    @Autowired
    public JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    WebSecuritySettings securitySettings;

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(securitySettings.getStrength());
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new LoyaltyAccessDeniedHandler();
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * This method configures that the /login is the only non protected resource,
     * and all the rest is protected. It also configures the application by setting a filter to
     * intercept requests and do required logic
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
                .accessDeniedHandler(accessDeniedHandler()).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
                .antMatchers("/", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html",
                        "/**/*.css", "/**/*.js", "/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html**", "/webjars/**", "/ws/**", "/socket/**").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/login", "/v1/generation/**", "/api/auth/refresh-token", "/api/auth/set-password").permitAll()
                .and()
                .authorizeRequests()
                .anyRequest().authenticated();
        // Add our custom JWT security filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}
