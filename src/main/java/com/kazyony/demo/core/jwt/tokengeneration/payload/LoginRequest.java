package com.kazyony.demo.core.jwt.tokengeneration.payload;

import javax.validation.constraints.NotBlank;

/**
 * @author Hassan Wael.
 */
public class LoginRequest {

    @NotBlank
    private String userName;

    @NotBlank
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginRequest [userName=" + userName + ", password=####" +"]";
    }
}
