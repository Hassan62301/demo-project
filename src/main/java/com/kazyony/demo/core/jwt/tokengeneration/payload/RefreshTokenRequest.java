package com.kazyony.demo.core.jwt.tokengeneration.payload;

import javax.validation.constraints.NotBlank;

/**
 * @author Hassan Wael.
 */
public class RefreshTokenRequest {

    @NotBlank
    private String refreshToken;

    public String getRefreshToken() { return refreshToken; }

    public void setRefreshToken(String refreshToken) { this.refreshToken = refreshToken; }

    @Override
    public String toString() {
        return "RefreshTokenRequest{" +
                "refreshToken='" + refreshToken + '\'' +
                '}';
    }
}
