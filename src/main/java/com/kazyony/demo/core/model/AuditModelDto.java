package com.kazyony.demo.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kazyony.demo.constants.LoyaltyConstants;

import java.time.LocalDateTime;

/**
 * @author Hassan Wael.
 */
public class AuditModelDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = LoyaltyConstants.HOST_DATE_TIME_FORMAT)
    private LocalDateTime createdDate;

    private String createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = LoyaltyConstants.HOST_DATE_TIME_FORMAT)
    private LocalDateTime lastModifiedDate;

    private String lastModifiedBy;

    private int version;

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "AuditModelDto{" +
                "createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", version=" + version +
                '}';
    }
}
