package com.kazyony.demo.core.model;

import java.util.Collection;

public class FilterDto {
    private String field;
    private String operation;
    private Collection<String> value;

    public FilterDto(String field, String operation, Collection<String> values, boolean isLast) {
        this.field = field;
        this.operation = operation;
        this.value = values;
    }

    public FilterDto() {
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Collection<String> getValue() {
        return value;
    }

    public void setValue(Collection<String> value) {
        this.value = value;
    }
}
