package com.kazyony.demo.core.config;

import com.kazyony.demo.core.jwt.tokenvalidation.model.UserPrincipal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

/**
 * @author Hassan Wael.
 */
@Configuration
@EnableJpaAuditing
public class AuditConfig {

    @Bean
    public AuditorAware<String> createAuditorProvider() {
        return new SecurityAuditor();
    }

    @Bean
    public AuditingEntityListener createAuditingListener() {
        return new AuditingEntityListener();
    }

    public static class SecurityAuditor implements AuditorAware<String> {

        @Override
        public Optional<String> getCurrentAuditor() {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = "SuperUser";
            if (auth != null && auth.getPrincipal() instanceof UserPrincipal) {
                username = ((UserPrincipal) auth.getPrincipal()).getUsername();
            }

            return Optional.of(username);
        }
    }
}
