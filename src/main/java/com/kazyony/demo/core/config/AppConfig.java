package com.kazyony.demo.core.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Hassan Wael.
 *
 * Class that contains all app configuration needed
 */
@Configuration
@EnableScheduling
public class AppConfig {

    // ModelMapper configuration used for mapping dtos to entity and vise versa
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
