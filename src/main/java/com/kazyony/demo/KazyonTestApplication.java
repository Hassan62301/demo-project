package com.kazyony.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class KazyonTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(KazyonTestApplication.class, args);

		List<String> splitted = new ArrayList<>(Arrays.asList("Thequickbrownfoxjumps".split("(?<=\\G.{10})")));

		System.out.println(splitted);

		String combined = String.join("", splitted);

		System.out.println(combined);

	}


}
