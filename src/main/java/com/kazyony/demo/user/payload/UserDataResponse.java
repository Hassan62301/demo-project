package com.kazyony.demo.user.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kazyony.demo.constants.LoyaltyConstants;
import com.kazyony.demo.user.dto.ApiUserDto;

import java.time.LocalTime;

/**
 * @author Hassan Wael
 */
public class UserDataResponse {

    @JsonProperty("user")
    private ApiUserDto apiUserDto;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = LoyaltyConstants.HOST_TIME_FORMAT)
    private LocalTime responseTime;

    public UserDataResponse() {
    }

    public UserDataResponse(ApiUserDto apiUserDto) {
        this.apiUserDto = apiUserDto;
    }

    public UserDataResponse(ApiUserDto apiUserDto, LocalTime responseTime) {
        this.apiUserDto = apiUserDto;
        this.responseTime = responseTime;
    }

    public ApiUserDto getApiUserDto() {
        return apiUserDto;
    }

    public void setApiUserDto(ApiUserDto apiUserDto) {
        this.apiUserDto = apiUserDto;
    }


    public LocalTime getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(LocalTime responseTime) {
        this.responseTime = responseTime;
    }

    @Override
    public String toString() {
        return "UserDataResponse{" +
                "apiUserDto=" + apiUserDto +
                ", responseTime=" + responseTime +
                '}';
    }
}
