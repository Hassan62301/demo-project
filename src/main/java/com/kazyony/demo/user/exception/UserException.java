package com.kazyony.demo.user.exception;

/**
 * @author Mohamed Waheed.
 * @date 7/22/2020
 */
public class UserException extends Exception {

    public UserException() {
    }

    public UserException(String message) {
        super(message);
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }
}
