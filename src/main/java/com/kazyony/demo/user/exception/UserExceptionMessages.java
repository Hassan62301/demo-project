package com.kazyony.demo.user.exception;

/**
 * @author Hassan Wael
 */
public class UserExceptionMessages {

    // user cannot be null
    public static final String API_NULL_USER = "API_NULL_USER";

    // id cannot be null or empty
    public static final String API_NULL_USER_ID = "API_NULL_USER_ID";

    // user password cannot be null
    public static final String API_NULL_USER_PASSWORD = "API_NULL_USER_PASSWORD";

    // username cannot be null
    public static final String API_NULL_USER_NAME = "API_NULL_USER_NAME";

    // user not found
    public static final String API_USER_NOT_FOUND = "API_USER_NOT_FOUND";

    // username cannot be found
    public static final String API_USER_NAME_NOT_FOUND = "API_USER_NAME_NOT_FOUND";

    // role cannot be null
    public static final String API_NULL_USER_ROLE = "API_NULL_USER_ROLE";

    // username already exists
    public static final String API_EXISTING_USERNAME = "API_EXISTING_USERNAME";

    // emailAddress already exists
    public static final String API_EXISTING_EMAIL = "API_EXISTING_EMAIL";

    // username or emailAddress already exists
    public static final String API_EXISTING_USERNAME_AND_EMAIL = "API_EXISTING_USERNAME_AND_EMAIL";

    // back_office user cannot be null
    public static final String API_NULL_BACK_OFFICE = "API_NULL_BACK_OFFICE";

    // back_office user not found in db
    public static final String API_NOT_IN_DB_BACK_OFFICE = "API_NOT_IN_DB_BACK_OFFICE";

    // email cannot be null
    public static final String API_NULL_EMAIL_BACK_OFFICE = "API_NULL_EMAIL_BACK_OFFICE";

    // user type cannot be null
    public static final String API_NULL_USER_TYPE = "API_NULL_USER_TYPE";

    // role userType is not matched with user UserType
    public static final String API_NOT_MATCHING_ROLES = "API_NOT_MATCHING_ROLES";

    // userType in role cannot be null
    public static final String API_NULL_USER_TYPE_IN_ROLE = "API_NULL_USER_TYPE_IN_ROLE";

    // role id cannot be null
    public static final String API_NULL_USER_ROLE_ID = "API_NULL_USER_ROLE_ID";

    // User not found by username
    public static final String API_NOT_FOUND_USER_BY_USER_NAME = "API_NOT_FOUND_USER_BY_USER_NAME";

    //duplicate username
    public static final String API_DUPLICATE_USERNAME = "API_DUPLICATE_USERNAME";

    // current password is not equal in database
    public static final String API_CURRENT_PASSWORD_NOT_EQUAL = "API_CURRENT_PASSWORD_NOT_EQUAL";

    public static final String API_SAME_STATUS_IN_DB = "API_SAME_STATUS_IN_DB";
}
