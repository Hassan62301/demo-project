package com.kazyony.demo.user.entity;

/**
 * @author Hassan Wael
 */
public enum ActivityType {

    ENABLED("ENABLED"),

    DISABLED("DISABLED"),

    PASSWORD_REST("PASSWORD_REST");

    private String action;

    ActivityType(String action) {
        this.action = action;
    }
}
