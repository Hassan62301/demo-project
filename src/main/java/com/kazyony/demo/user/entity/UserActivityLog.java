package com.kazyony.demo.user.entity;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Hassan Wael
 */
@Entity
@Table(name = UserActivityLog.TABLE_NAME)
@TypeDef(name = "activity_type", typeClass = PostgreSQLEnumType.class)
public class UserActivityLog {

    static final String TABLE_NAME = "USER_ACTIVITY_LOG";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Type(type = "activity_type")
    @Enumerated(EnumType.STRING)
    @Column(name = "ACTIVITY_TYPE", length = 20, nullable = false)
//    @Column(name = "ACTIVITY_TYPE", columnDefinition = "activity_type", length = 20, nullable = false)
    private ActivityType activityType;

    @Column(name = "PERFORMED_BY")
    private String performedBy;

    @Column(name = "PERFORMED_AT")
    private LocalDateTime performedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public String getPerformedBy() {
        return performedBy;
    }

    public void setPerformedBy(String performedBy) {
        this.performedBy = performedBy;
    }

    public LocalDateTime getPerformedAt() {
        return performedAt;
    }

    public void setPerformedAt(LocalDateTime performedAt) {
        this.performedAt = performedAt;
    }

    @Override
    public String toString() {
        return "UserActivityLog{" +
                "id=" + id +
                ", activityType=" + activityType +
                ", performedBy='" + performedBy + '\'' +
                ", performedAt=" + performedAt +
                '}';
    }
}
