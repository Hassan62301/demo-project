package com.kazyony.demo.user.entity;

/**
 * @author Hassan Wael
 */
public enum UserStatus {

    AVAILABLE("AVAILABLE"),

    UNAVAILABLE("UNAVAILABLE");

    private String status;

    UserStatus(String status) {
        this.status = status;
    }
}
