package com.kazyony.demo.user.entity;

import com.kazyony.demo.core.model.AuditModel;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.user.constants.UserType;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hassan Wael
 */
@Entity
@Table(name = User.TABLE_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@TypeDef(name = "user_type", typeClass = PostgreSQLEnumType.class, defaultForType = UserType.class)
public class User extends AuditModel {

    static final String TABLE_NAME = "`USER`";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;

    @NotNull
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotNull
    @Column(name = "LAST_NAME")
    private String lastName;

    @Type(type = "user_type")
    @Enumerated(EnumType.STRING)
    @Column(name = "USER_TYPE", length = 20, nullable = false)
    private UserType userType;

    @NotNull
    @Column(name = "USER_NAME", nullable = false, unique = true)
    private String userName;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    private boolean initialPassword = true;

    @Column(name = "ENABLED")
    private boolean enabled;

    @ManyToOne
    @JoinColumn(name = "ROLE_ID", nullable = false)
    private Role role;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @OrderBy("id desc ")
    private List<UserActivityLog> userActivityLogs;

    /* Constructors */

    public User() {
    }

    public User(String firstName, String lastName, String userName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.enabled = true;
        this.userActivityLogs = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isInitialPassword() {
        return initialPassword;
    }

    public void setInitialPassword(boolean initialPassword) {
        this.initialPassword = initialPassword;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<UserActivityLog> getUserActivityLogs() {
        return userActivityLogs;
    }

    public void setUserActivityLogs(List<UserActivityLog> userActivityLogs) {
        this.userActivityLogs = userActivityLogs;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userType='" + userType + '\'' +
                ", userName='" + userName + '\'' +
                ", initialPassword='" + initialPassword + '\'' +
                ", enabled=" + enabled +
                ", role=" + ((role != null) ? role.getId() : "") +
                '}';
    }
}
