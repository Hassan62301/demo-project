package com.kazyony.demo.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Hassan Wael
 */
@Entity
@Table(name = BackOffice.TABLE_NAME)
public class BackOffice extends User {

    static final String TABLE_NAME = "BACK_OFFICE";

    @NotNull
    @Column(name = "EMAIL_ADDRESS", unique = true)
    private String emailAddress;

    /* Constructors */

    public BackOffice() {
    }

    public BackOffice(String firstName, String lastName, String userName, String password) {
        super(firstName, lastName, userName, password);
    }

    /* Getters and Setters */

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "BackOffice{" +
                "emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
