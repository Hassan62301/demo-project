package com.kazyony.demo.user.service;

import com.kazyony.demo.core.errorhandling.LoyaltyBadRequestException;
import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.dto.ApiUserDto;
import com.kazyony.demo.user.dto.BackOfficeDto;
import com.kazyony.demo.user.dto.UserDto;
import com.kazyony.demo.user.entity.ActivityType;
import com.kazyony.demo.user.entity.BackOffice;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.user.entity.UserActivityLog;
import com.kazyony.demo.user.exception.UserException;
import com.kazyony.demo.user.exception.UserExceptionMessages;
import com.kazyony.demo.user.mapper.UserMapper;
import com.kazyony.demo.user.payload.UserDataResponse;
import com.kazyony.demo.user.repository.BackOfficeRepository;
import com.kazyony.demo.user.repository.UserRepository;
import com.kazyony.demo.util.PageRequestBuilder;
import com.kazyony.demo.util.PasswordEncodingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

/**
 * @author Hassan Wael
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    PasswordEncodingHelper passwordEncodingHelper;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BackOfficeRepository backOfficeRepository;
    @Autowired
    private UserMapper userMapper;


    /* method service to set new password */
    @Override
    public User setNewPassword(User user, String newPassword) throws Exception {
        logger.info("##-- setNewPassword method is called --##");
        user.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt(newPassword));
        user.setInitialPassword(false);
        logger.info("##-- new password is added and set InitialPassword to false --##");
        return userRepository.save(user);
    }

    @Override
    public UserDataResponse getUserDataResponse(User user) {
        ApiUserDto apiUserDto = userMapper.convertEntityToApiUserDto(user);
        logger.info("##-- user entity is converted to ApiUserDto --##");
        return new UserDataResponse(apiUserDto, LocalTime.now());
    }

    /* get all backOffice Users paginated */
    @Override
    public Page<BackOfficeDto> findAllBackOfficeDto(Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException {
        Page<BackOffice> backOffices = backOfficeRepository.findAll(PageRequestBuilder.build(pageNumber, pageSize, sortField, sortOrder));
        return backOffices.map(backOffice -> userMapper.convertToBackOfficeDto(backOffice));
    }

    /* get all backOffice Users paginated with filtered value */
    @Override
    public Page<BackOfficeDto> findAllBackOfficeDto(Specification<BackOffice> specification, Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException {
        Page<BackOffice> backOffices = backOfficeRepository.findAll(specification, PageRequestBuilder.build(pageNumber, pageSize, sortField, sortOrder));
        return backOffices.map(backOffice -> userMapper.convertToBackOfficeDto(backOffice));
    }

    /* find User by id */
    @Override
    public User findUserById(Long id) {
        if (id == null) {
            logger.info("##-- user id is null --##");
            throw new LoyaltyBadRequestException(UserExceptionMessages.API_NULL_USER_ID);
        }
        return userRepository.findById(id).orElse(null);
    }

    /* find User by UserName */
    @Override
    public User findByUserName(String userName) {
        if (userName == null) {
            logger.info("##-- userName is null --##");
            throw new LoyaltyBadRequestException(UserExceptionMessages.API_NULL_USER_NAME);
        }
        return userRepository.findByUserName(userName).orElse(null);
    }

    /* Create new User */
    @Override
    public User createUser(User user) throws UserException {
        if (user == null) {
            logger.info("##-- User Entity is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER);
        }
        if (user.getRole() == null) {
            logger.info("##-- User Role is null or not found --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ROLE);
        }
        Optional<User> userInDb = userRepository.findByUserName(user.getUserName());
        if (userInDb.isPresent()) {
            logger.info("##-- user have same username or email in database --##");
            throw new UserException(UserExceptionMessages.API_EXISTING_USERNAME_AND_EMAIL);
        }
        String passwordEncrypted = passwordEncodingHelper.encodePasswordUsingBcrypt(user.getPassword());
        user.setPassword(passwordEncrypted);
        user.setEnabled(true);
        user.setInitialPassword(true);
        logger.info("##-- New User is created --##");
        return userRepository.save(user);
    }

    /* Create new BackOffice User */
    @Override
    public BackOffice createBackOfficeUser(BackOffice backOffice) throws UserException {
        backOffice.setUserType(UserType.BACK_OFFICE);
        Role role = backOffice.getRole();
        if(role == null){
            logger.info("##-- user role is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ROLE);
        }
        if (role.getId() == null) {
            logger.info("##-- user role id is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ROLE_ID);
        }
        if (!backOffice.getUserType().equals(role.getUserType())) {
            logger.info("##-- userType is not matched as in Role --##");
            throw new UserException(UserExceptionMessages.API_NOT_MATCHING_ROLES);
        }
        List<User> users = userRepository.findAllByUserName(backOffice.getUserName());
        if (!users.isEmpty()) {
            logger.info("##-- username exist in database --##");
            throw new UserException(UserExceptionMessages.API_EXISTING_USERNAME);
        }
        BackOffice userInDb = backOfficeRepository.findByEmailAddress(backOffice.getEmailAddress());
        if (userInDb != null) {
            logger.info("##--  user email exist in database --##");
            throw new UserException(UserExceptionMessages.API_EXISTING_EMAIL);
        }
        String passwordEncrypted = passwordEncodingHelper.encodePasswordUsingBcrypt(backOffice.getPassword());
        backOffice.setPassword(passwordEncrypted);
        backOffice.setEnabled(true);
        backOffice.setUserType(UserType.BACK_OFFICE);
        backOffice.setInitialPassword(true);
        logger.info("##-- new BackOffice user is created --##");
        return userRepository.save(backOffice);
    }

    /* Update BackOffice User */
    @Override
    @Transactional
    public BackOffice updateBackOfficeUser(BackOffice backOffice, Long id) throws UserException {
        if (backOffice == null) {
            logger.info("##--  backOffice user is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_BACK_OFFICE);
        } else if (id == null) {
            logger.info("##-- backOffice user id is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ID);
        } else if (backOffice.getRole() == null) {
            logger.info("##-- backOffice user role is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ROLE);
        }
        BackOffice backOfficeInDb = backOfficeRepository.findByEmailAddressAndIdNot(backOffice.getEmailAddress(), id);
        if (backOfficeInDb != null) {
            logger.info("##--  user email exist in database --##");
            throw new UserException(UserExceptionMessages.API_EXISTING_EMAIL);
        }
        BackOffice oldBackOffice = (BackOffice) findUserById(id);
        if (oldBackOffice == null) {
            throw new UserException(UserExceptionMessages.API_NOT_IN_DB_BACK_OFFICE);
        }

        oldBackOffice.setFirstName(backOffice.getFirstName());
        oldBackOffice.setLastName(backOffice.getLastName());
        oldBackOffice.setRole(backOffice.getRole());
        oldBackOffice.setEmailAddress(backOffice.getEmailAddress());
        logger.info("##--  BackOffice User is updated --##");

        return userRepository.save(oldBackOffice);
    }

    /**
     * Responsible of building ApiUserDto obj for any integrator
     *
     * @param user User
     * @return ApiUserDto
     */
    @Override
    public ApiUserDto getApiUserDtoFromUser(User user) throws Exception {
        return userMapper.convertEntityToApiUserDto(user);
    }

    /* find backOffice user by id */
    @Override
    public BackOffice findBackOfficeUserById(Long userId) throws UserException {
        if(userId == null) {
            logger.info("##-- backOffice user id is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ID);
        }
        BackOffice backOffice = backOfficeRepository.findById(userId).orElse(null);
        if(backOffice == null) {
            logger.info("##--  backOffice user is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_BACK_OFFICE);
        }
        logger.info("##--  backOffice user with id {} is found --##", userId);
        return backOffice;
    }

    /* toggle user status to deactivate or activate user */
    @Override
    public User toggleUserStatus(Boolean toggleStatus, Long userId) throws UserException {
        if(userId == null) {
            logger.info("##-- backOffice user id is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER_ID);
        }
        User userInDb = userRepository.findById(userId).orElse(null);
        if(userInDb == null) {
            logger.info("##--  backOffice user is null --##");
            throw new UserException(UserExceptionMessages.API_NULL_USER);
        }
        if (userInDb.isEnabled() == toggleStatus) {
            logger.info("##--  backOffice user in db has same status specified --##");
            throw new UserException(UserExceptionMessages.API_SAME_STATUS_IN_DB);
        }
        logger.info("##--  backOffice user has been changed to {} --##", toggleStatus);
        return changeUserStatus(toggleStatus, userInDb);
    }

    /* change and save new user status */
    private User changeUserStatus(Boolean toggleStatus, User userInDb) {
        userInDb.setEnabled(toggleStatus);
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();
        UserActivityLog userActivityLog = new UserActivityLog();
        if (toggleStatus) {
            userActivityLog.setActivityType(ActivityType.ENABLED);
        } else {
            userActivityLog.setActivityType(ActivityType.DISABLED);
        }
        userActivityLog.setPerformedBy(username);
        userActivityLog.setPerformedAt(LocalDateTime.now());
        userInDb.getUserActivityLogs().add(userActivityLog);
        userInDb = userRepository.save(userInDb);
        return userInDb;
    }
}
