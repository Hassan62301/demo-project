package com.kazyony.demo.user.service;

import com.kazyony.demo.user.dto.ApiUserDto;
import com.kazyony.demo.user.dto.BackOfficeDto;
import com.kazyony.demo.user.dto.UserDto;
import com.kazyony.demo.user.entity.BackOffice;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.user.exception.UserException;
import com.kazyony.demo.user.payload.UserDataResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

/**
 * @author Hassan Wael
 */
public interface UserService {

    /* method service to set new password */
    User setNewPassword(User user, String newPassword) throws Exception;

    UserDataResponse getUserDataResponse(User user);

    /* get all backOffice Users paginated */
    Page<BackOfficeDto> findAllBackOfficeDto(Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException;

    /* get all backOffice Users paginated with filtered value */
    Page<BackOfficeDto> findAllBackOfficeDto(Specification<BackOffice> specification, Integer pageNumber, Integer pageSize, String sortField, Integer sortOrder) throws IOException;

    /* find User by id */
    User findUserById(Long id);

    /* find User by UserName */
    User findByUserName(String userName);

    /* Create new User */
    User createUser(User user) throws UserException;

    /* Create new BackOffice User */
    BackOffice createBackOfficeUser(BackOffice backOffice) throws UserException;

    /* Update BackOffice User */
    BackOffice updateBackOfficeUser(BackOffice backOffice, Long id) throws UserException;

    /* return ApiUserDto from User */
    ApiUserDto getApiUserDtoFromUser(User user) throws Exception;

    /* find backOffice user by id */
    BackOffice findBackOfficeUserById(Long userId) throws UserException;

    /* toggle user status to deactivate or activate user */
    User toggleUserStatus(Boolean toggleStatus, Long userId) throws UserException;

}
