package com.kazyony.demo.user.mapper;

import com.kazyony.demo.role.mapper.RoleMapper;
import com.kazyony.demo.user.dto.ApiUserDto;
import com.kazyony.demo.user.dto.BackOfficeDto;
import com.kazyony.demo.user.dto.UserDto;
import com.kazyony.demo.user.entity.BackOffice;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.util.TrimConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Hassan Wael
 */
@Component
public class UserMapper {

    @Autowired
    TrimConverter trimConverter;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RoleMapper roleMapper;

    /* Convert User to UserDto */
    public UserDto convertEntityToDto(User user) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(user, UserDto.class);
    }

    /* Convert UserDto to User */
    public User convertDtoToEntity(UserDto userDto) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(userDto, User.class);
    }

    /* Convert List of User to List of UserDto */
    public List<UserDto> convertUserListToDto(List<User> users) {
        modelMapper.addConverter(trimConverter.trimData());
        return users.stream()
                .map(user -> modelMapper.map(user, UserDto.class))
                .collect(Collectors.toList());
    }

    /* Convert List of BackOffice to List of BackOfficeDto */
    public BackOfficeDto convertToBackOfficeDto(BackOffice backOffice) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(backOffice, BackOfficeDto.class);
    }

    /* Convert List of BackOfficeDto to List of BackOffice */
    public BackOffice convertBackOfficeToEntity(BackOfficeDto backOfficeDto) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(backOfficeDto, BackOffice.class);
    }

    /* Convert List of BackOffice to List of BackOfficeDto */
    public List<BackOfficeDto> convertBackOfficeListToDto(List<BackOffice> backOfficeDtoList) {
        modelMapper.addConverter(trimConverter.trimData());
        return backOfficeDtoList.stream()
                .map(user -> modelMapper.map(user, BackOfficeDto.class))
                .collect(Collectors.toList());
    }

    /* Convert User to ApiUserDto */
    public ApiUserDto convertEntityToApiUserDto(User user) {
        modelMapper.addConverter(trimConverter.trimData());
        return modelMapper.map(user, ApiUserDto.class);
    }

}
