package com.kazyony.demo.user.constants;

/**
 * @author Hassan Wael.
 */
public enum UserType {

    /*Users with access to the Loyalty portal*/
    BACK_OFFICE("BACK_OFFICE"),

    /*Users with super access to the Loyalty portal*/
    SUPERUSER("SUPERUSER");

    private String userType;

    UserType(String userType) {
        this.userType = userType;
    }

}
