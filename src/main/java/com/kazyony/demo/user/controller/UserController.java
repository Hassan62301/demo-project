package com.kazyony.demo.user.controller;

import com.kazyony.demo.core.errorhandling.ErrorResponse;
import com.kazyony.demo.core.errorhandling.LoyaltyBadRequestException;
import com.kazyony.demo.core.jwt.exception.AuthenticationExceptionMessages;
import com.kazyony.demo.core.model.FilterDto;
import com.kazyony.demo.core.specification.SearchSpecification;
import com.kazyony.demo.permission.constants.PermissionsLiterals;
import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.role.exception.RoleExceptionMessages;
import com.kazyony.demo.user.dto.BackOfficeDto;
import com.kazyony.demo.user.dto.UserDto;
import com.kazyony.demo.user.entity.BackOffice;
import com.kazyony.demo.user.entity.BackOffice_;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.user.exception.UserExceptionMessages;
import com.kazyony.demo.user.mapper.UserMapper;
import com.kazyony.demo.user.service.UserService;
import com.kazyony.demo.util.FiltersParser;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author Hassan Wael.
 */
@RestController
@RequestMapping("v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private SearchSpecification<BackOffice, BackOffice_> searchSpecificationBackOffice;

    @Autowired
    private UserMapper userMapper;


    /* endpoint to get all backOffice Users */
    @GetMapping("/backOffice")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_ALL_ACTIONS,
            PermissionsLiterals.BACKOFFICE_VIEW, PermissionsLiterals.BACKOFFICE_VIEW_AND_ADD,
            PermissionsLiterals.BACKOFFICE_VIEW_AND_UPDATE})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class)
    })
    public ResponseEntity<?> getAllBackOfficeUsers(@RequestParam(value = "pageNum", required = false) Integer pageNumber,
                                                   @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                                   @RequestParam(value = "sortField", required = false) String sortField,
                                                   @RequestParam(value = "sortOrder", required = false) Integer sortOrder,
                                                   @RequestParam(value = "filters", required = false) String filters,
                                                   @RequestHeader("accept") String acceptHeader) throws IOException {

        Page<BackOfficeDto> usersList;
        if (filters != null && !filters.isEmpty()) {
            List<FilterDto> filterDtoList = FiltersParser.parseFilters(filters);
            Specification<BackOffice> searchSpec = searchSpecificationBackOffice.filter(filterDtoList, BackOffice_.class);
            usersList = userService.findAllBackOfficeDto(searchSpec, pageNumber, pageSize, sortField, sortOrder);
        } else {
            usersList = userService.findAllBackOfficeDto(pageNumber, pageSize, sortField, sortOrder);
        }
        return new ResponseEntity<>(usersList, null, HttpStatus.OK);
    }


    /* endpoint to find backOffice User by id */
    @GetMapping("/backOffice/{userId}")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_VIEW_AND_ADD})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = UserExceptionMessages.API_NULL_BACK_OFFICE + ", " +
                    UserExceptionMessages.API_NULL_USER_ROLE + ", " + UserExceptionMessages.API_NULL_USER_ROLE_ID, response = ErrorResponse.class)
    })
    public ResponseEntity<?> getBackOfficeUserById(@PathVariable Long userId) {
        try {
            BackOffice result = userService.findBackOfficeUserById(userId);
            BackOfficeDto resultDto = userMapper.convertToBackOfficeDto(result);
            return new ResponseEntity<>(resultDto, null, HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /* endpoint to Create new backOffice User */
    @PostMapping("/backOffice")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_VIEW_AND_ADD})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = UserExceptionMessages.API_NULL_BACK_OFFICE + ", " +
                    UserExceptionMessages.API_NULL_USER_ROLE + ", " + UserExceptionMessages.API_NULL_USER_ROLE_ID +
                    ", " + UserExceptionMessages.API_NOT_MATCHING_ROLES + ", " + UserExceptionMessages.API_EXISTING_USERNAME +
                    ", " + UserExceptionMessages.API_EXISTING_EMAIL, response = ErrorResponse.class)
    })
    public ResponseEntity<?> createBackOfficeUser(@Valid @RequestBody BackOfficeDto backOfficeDto) {
        try {
            if (backOfficeDto == null) {
                throw new LoyaltyBadRequestException(UserExceptionMessages.API_NULL_BACK_OFFICE);
            }
            BackOffice backOffice = userMapper.convertBackOfficeToEntity(backOfficeDto);
            BackOffice result = userService.createBackOfficeUser(backOffice);
            BackOfficeDto resultDto = userMapper.convertToBackOfficeDto(result);
            return new ResponseEntity<>(resultDto, null, HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /* endpoint to Update backOffice User */
    @PutMapping("/backOffice/{userId}")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_VIEW_AND_UPDATE})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = UserExceptionMessages.API_NULL_BACK_OFFICE + ", " +
                    UserExceptionMessages.API_NULL_USER_ROLE + ", " + UserExceptionMessages.API_NULL_USER_ROLE_ID +
                    ", " + UserExceptionMessages.API_NOT_MATCHING_ROLES + ", " + UserExceptionMessages.API_EXISTING_EMAIL, response = ErrorResponse.class)})
    public ResponseEntity<?> updateBackOfficeUser(@PathVariable Long userId, @RequestBody BackOfficeDto backOfficeDto) {
        try {
            if (backOfficeDto == null) {
                throw new LoyaltyBadRequestException(UserExceptionMessages.API_NULL_BACK_OFFICE);
            }
            BackOffice backOffice = userMapper.convertBackOfficeToEntity(backOfficeDto);
            BackOffice result = userService.updateBackOfficeUser(backOffice, userId);
            BackOfficeDto resultDto = userMapper.convertToBackOfficeDto(result);
            return new ResponseEntity<>(resultDto, null, HttpStatus.OK);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }


    /* endpoint to Create User (Super-User) */
    @PostMapping
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = UserExceptionMessages.API_NULL_USER + ", " +
                    UserExceptionMessages.API_NULL_USER_ROLE + ", " + UserExceptionMessages.API_EXISTING_USERNAME_AND_EMAIL, response = ErrorResponse.class)})
    public ResponseEntity<?> createUser(@Valid @RequestBody UserDto userDto) {
        try {
            if (userDto == null) {
                throw new LoyaltyBadRequestException(UserExceptionMessages.API_NULL_USER);
            }
            User user = userMapper.convertDtoToEntity(userDto);
            User result = userService.createUser(user);
            UserDto resultDto = userMapper.convertEntityToDto(result);
            return new ResponseEntity<>(resultDto, null, HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

    /* endpoint to toggle user status (activate or deactivate user) */
    @PatchMapping("/toggle/{userId}")
    @Secured({PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, PermissionsLiterals.BACKOFFICE_ALL_ACTIONS, PermissionsLiterals.USER_CHANGE_STATUS})
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RoleDto.class, message = "OK"),
            @ApiResponse(code = 403, message = AuthenticationExceptionMessages.API_INACTIVE_USER, response = ErrorResponse.class),
            @ApiResponse(code = 401, message = AuthenticationExceptionMessages.API_ACCESS_NOT_ALLOWED, response = ErrorResponse.class),
            @ApiResponse(code = 400, message = UserExceptionMessages.API_NULL_USER_ID + ", " +
                    UserExceptionMessages.API_NULL_USER + ", " + UserExceptionMessages.API_SAME_STATUS_IN_DB, response = ErrorResponse.class)})
    public ResponseEntity<?> toggleUserStatus(@RequestParam Boolean toggleStatus, @PathVariable Long userId) {
        try {
            User result = userService.toggleUserStatus(toggleStatus, userId);
            UserDto resultDto = userMapper.convertEntityToDto(result);
            return new ResponseEntity<>(resultDto, null, HttpStatus.OK);
        } catch (Exception ex) {
            throw new LoyaltyBadRequestException(ex.getMessage());
        }
    }

}
