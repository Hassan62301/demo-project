package com.kazyony.demo.user.controller;

import com.kazyony.demo.permission.constants.PermissionsLiterals;
import com.kazyony.demo.permission.entity.Permission;
import com.kazyony.demo.permission.repository.PermissionRepository;
import com.kazyony.demo.role.entity.Role;
import com.kazyony.demo.role.repository.RoleRepository;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.entity.BackOffice;
import com.kazyony.demo.user.entity.User;
import com.kazyony.demo.user.repository.UserRepository;
import com.kazyony.demo.user.service.UserService;
import com.kazyony.demo.util.PasswordEncodingHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author Hassan Wael.
 */
@RestController
@RequestMapping("v1/generation")
public class UserGenerationController {


    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    PasswordEncodingHelper passwordEncodingHelper;


    @PostMapping("/users")
    public ResponseEntity<?> usersGeneration() {
        createPermissions();
        createAdminAndSuperUserRoles();
        createSuperUser();
        createUsers();

        return new ResponseEntity<>("Done!", null, HttpStatus.OK);
    }

    private void createUsers() {
        Role admin = roleRepository.findByName("Admin");
        Role superRole = roleRepository.findByName("SuperRole");

        BackOffice user1 = new BackOffice();
        user1.setFirstName("Ahmed");
        user1.setLastName("Gamal");
        user1.setUserName("ahmed.gamal");
        user1.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("1234"));
        user1.setRole(admin);
        user1.setUserType(UserType.BACK_OFFICE);
        user1.setEnabled(true);
        user1.setEmailAddress("ahmed.gamal@brightskiesinc.com");

        BackOffice user2 = new BackOffice();
        user2.setFirstName("Ahmed");
        user2.setLastName("Salah");
        user2.setUserName("ahmed.salah");
        user2.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("1234"));
        user2.setRole(admin);
        user2.setEnabled(true);
        user2.setUserType(UserType.BACK_OFFICE);
        user2.setEmailAddress("ahmed.salah@brightskiesinc.com");

        BackOffice user3 = new BackOffice();
        user3.setFirstName("Marina");
        user3.setLastName("Yoaqoub");
        user3.setUserName("marina.yaqoub");
        user3.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("1234"));
        user3.setRole(admin);
        user3.setEnabled(true);
        user3.setUserType(UserType.BACK_OFFICE);
        user3.setEmailAddress("marina.yaqoub@brightskiesinc.com");

        BackOffice user4 = new BackOffice();
        user4.setFirstName("Khaled");
        user4.setLastName("Adel");
        user4.setUserName("khaled.adel");
        user4.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("1234"));
        user4.setRole(admin);
        user4.setUserType(UserType.BACK_OFFICE);
        user4.setEnabled(true);
        user4.setEmailAddress("khaled.adel@brightskiesinc.com");

        BackOffice user5 = new BackOffice();
        user5.setFirstName("Mostafa");
        user5.setLastName("Ibrahim");
        user5.setUserName("mostafa.ibrahim");
        user5.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("1234"));
        user5.setRole(superRole);
        user5.setUserType(UserType.BACK_OFFICE);
        user5.setEnabled(true);
        user5.setEmailAddress("mostafa.ibrahim@brightskiesinc.com");

        BackOffice user6 = new BackOffice();
        user6.setFirstName("Karim");
        user6.setLastName("Ahmed");
        user6.setUserName("karim.ahmed");
        user6.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("1234"));
        user6.setRole(superRole);
        user6.setUserType(UserType.BACK_OFFICE);
        user6.setEnabled(true);
        user6.setEmailAddress("karim.ahmed@brightskiesinc.com");

        userRepository.saveAll(Arrays.asList(user1, user2, user3, user4, user3, user4, user5, user6));
    }

    private void createPermissions() {
        /* SuperUser Permissions */
        List<Permission> allPermissions = new ArrayList<>();
        final Permission ALL_PAGES_ALL_ACTIONS = new Permission(PermissionsLiterals.ALL_PAGES_ALL_ACTIONS, "can do " +
                "anything");
        allPermissions.add(ALL_PAGES_ALL_ACTIONS);

        /*  Role Permissions */
        final Permission ROLE_VIEW = new Permission(PermissionsLiterals.ROLE_VIEW, "can view roles");
        allPermissions.add(ROLE_VIEW);
        final Permission ROLE_VIEW_AND_ADD = new Permission(PermissionsLiterals.ROLE_VIEW_AND_ADD, "can view and add " +
                "roles");
        allPermissions.add(ROLE_VIEW_AND_ADD);
        final Permission ROLE_VIEW_AND_UPDATE = new Permission(PermissionsLiterals.ROLE_VIEW_AND_UPDATE, "can view " +
                "and update roles");
        allPermissions.add(ROLE_VIEW_AND_UPDATE);
        final Permission ROLE_VIEW_AND_DELETE = new Permission(PermissionsLiterals.ROLE_VIEW_AND_DELETE, "can view " +
                "and delete roles");
        allPermissions.add(ROLE_VIEW_AND_DELETE);
        final Permission ROLE_ALL_ACTIONS = new Permission(PermissionsLiterals.ROLE_ALL_ACTIONS, "can do all actions " +
                "of roles");
        allPermissions.add(ROLE_ALL_ACTIONS);
        final Permission BACKOFFICE_VIEW = new Permission(PermissionsLiterals.BACKOFFICE_VIEW, "can view backoffice " +
                "users");

        /*  BackOffice Permissions */
        allPermissions.add(BACKOFFICE_VIEW);
        final Permission BACKOFFICE_VIEW_AND_ADD = new Permission(PermissionsLiterals.BACKOFFICE_VIEW_AND_ADD, "can " +
                "view and add backoffice users");
        allPermissions.add(BACKOFFICE_VIEW_AND_ADD);
        final Permission BACKOFFICE_VIEW_AND_UPDATE = new Permission(PermissionsLiterals.BACKOFFICE_VIEW_AND_UPDATE,
                "can view and update backoffice users");
        allPermissions.add(BACKOFFICE_VIEW_AND_UPDATE);
        final Permission BACKOFFICE_ALL_ACTIONS = new Permission(PermissionsLiterals.BACKOFFICE_ALL_ACTIONS, "can do " +
                "all backoffice actions");
        allPermissions.add(BACKOFFICE_ALL_ACTIONS);

        /* Common Permissions */
        final Permission USER_CHANGE_STATUS = new Permission(PermissionsLiterals.USER_CHANGE_STATUS, "can change " +
                "user status");
        allPermissions.add(BACKOFFICE_ALL_ACTIONS);

        permissionRepository.saveAll(allPermissions);


    }

    private void createAdminAndSuperUserRoles() {
        Set<Permission> superUserPermissions = new HashSet<>();
        Set<Permission> adminPermissions = new HashSet<>();

        //super user permissions
        final Permission ALL_PAGES_ALL_ACTIONS =
                permissionRepository.findByName(PermissionsLiterals.ALL_PAGES_ALL_ACTIONS);
        // admin permissions
        final Permission BACKOFFICE_ALL_ACTIONS =
                permissionRepository.findByName(PermissionsLiterals.BACKOFFICE_ALL_ACTIONS);


        Role superRole = new Role();
        superRole.setName("SuperRole");
        superRole.setUserType(UserType.SUPERUSER);
        superRole.setDescription("who can do anything!");
        superUserPermissions.add(ALL_PAGES_ALL_ACTIONS);
        superRole.setPermissions(superUserPermissions);


        Role admin = new Role();
        admin.setName("Admin");
        admin.setUserType(UserType.BACK_OFFICE);
        admin.setDescription("who can manage all pages");
        adminPermissions.add(BACKOFFICE_ALL_ACTIONS);
        admin.setPermissions(adminPermissions);

        roleRepository.saveAll(Arrays.asList(admin, superRole));

    }

    private void createSuperUser() {

        Role superRole = roleRepository.findByName("SuperRole");
        User user = new User();
        user.setUserName("SuperUser");
        user.setFirstName("Super");
        user.setLastName("User");
        user.setPassword(passwordEncodingHelper.encodePasswordUsingBcrypt("ls_kazyon2021"));
        user.setRole(superRole);
        user.setUserType(UserType.SUPERUSER);
        user.setEnabled(true);
        userRepository.save(user);

    }
}
