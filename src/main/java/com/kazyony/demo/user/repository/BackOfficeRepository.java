package com.kazyony.demo.user.repository;

import com.kazyony.demo.user.entity.BackOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Hassan Wael
 */
@Repository
public interface BackOfficeRepository extends JpaRepository<BackOffice, Long>, JpaSpecificationExecutor<BackOffice> {

    BackOffice findByEmailAddressAndIdNot(String emailAddress, Long id);

    BackOffice findByEmailAddress(String emailAddress);
}
