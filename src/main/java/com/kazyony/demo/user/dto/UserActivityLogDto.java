package com.kazyony.demo.user.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kazyony.demo.constants.LoyaltyConstants;
import com.kazyony.demo.user.entity.ActivityType;

import java.time.LocalDateTime;

/**
 * @author Hassan Wael
 */
public class UserActivityLogDto {

    private long id;

    private ActivityType activityType;

    private String performedBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = LoyaltyConstants.HOST_DATE_TIME_FORMAT)
    private LocalDateTime performedAt;

    /* Constructors */

    public UserActivityLogDto() {
    }

    public UserActivityLogDto(long id, ActivityType activityType, String performedBy, LocalDateTime performedAt) {
        this.id = id;
        this.activityType = activityType;
        this.performedBy = performedBy;
        this.performedAt = performedAt;
    }

    /* Getters and Setters */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public String getPerformedBy() {
        return performedBy;
    }

    public void setPerformedBy(String performedBy) {
        this.performedBy = performedBy;
    }

    public LocalDateTime getPerformedAt() {
        return performedAt;
    }

    public void setPerformedAt(LocalDateTime performedAt) {
        this.performedAt = performedAt;
    }

    @Override
    public String toString() {
        return "UserActivityLogDto{" +
                "id=" + id +
                ", activityType=" + activityType +
                ", performedBy='" + performedBy + '\'' +
                ", performedAt=" + performedAt +
                '}';
    }
}
