package com.kazyony.demo.user.dto;

import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.user.constants.UserType;

import javax.validation.constraints.NotBlank;

/**
 * @author Hassan Wael
 */
public class BackOfficeDto extends UserDto {

    @NotBlank
    private String emailAddress;

    /* Constructors */

    public BackOfficeDto() {
        super();
    }

    public BackOfficeDto(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public BackOfficeDto(long id, String firstName, String lastName, UserType userType, String userName, String password,
                         boolean enabled, RoleDto role, String emailAddress) {
        super(id, firstName, lastName, userType, userName, password, enabled, role);
        this.emailAddress = emailAddress;
    }

    /* Getters and Setters */

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "BackOfficeDto{" +
                "emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
