package com.kazyony.demo.user.dto;

import com.kazyony.demo.permission.dto.PermissionDto;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Hassan Wael
 */
public class CreateRoleRequestDto {

    private Long id;

    private String description;

    private String name;

    private Set<PermissionDto> permissions;

    public CreateRoleRequestDto() {
        super();
        permissions = new HashSet<>();
    }

    public CreateRoleRequestDto(Long id, String description, String name, Set<PermissionDto> permissions) {
        this();
        this.id = id;
        this.description = description;
        this.name = name;
        this.permissions = permissions;
    }

    public CreateRoleRequestDto(String description, String name, Set<PermissionDto> permissions) {
        this();
        this.description = description;
        this.name = name;
        this.permissions = permissions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PermissionDto> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<PermissionDto> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "CreateRoleRequestDto{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
