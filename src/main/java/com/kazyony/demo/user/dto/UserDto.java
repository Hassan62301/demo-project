package com.kazyony.demo.user.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.kazyony.demo.core.model.AuditModelDto;
import com.kazyony.demo.role.dto.RoleDto;
import com.kazyony.demo.user.constants.UserType;
import com.kazyony.demo.user.resolver.UserIdResolver;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hassan Wael
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",
        resolver = UserIdResolver.class, scope = UserDto.class)
public class UserDto extends AuditModelDto implements Serializable {

    private long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    private UserType userType;

    @NotBlank
    private String userName;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private boolean enabled;

    @JsonIdentityReference(alwaysAsId = true)
    private RoleDto role;

    private boolean initialPassword;

    private List<UserActivityLogDto> userActivityLogs;

    /* Constructors */

    public UserDto() {
        super();
        userActivityLogs = new ArrayList<>();
    }

    public UserDto(long id, String firstName, String lastName, UserType userType, String userName, String password,
                   boolean enabled, RoleDto role) {
        this();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userType = userType;
        this.userName = userName;
        this.password = password;
        this.enabled = enabled;
        this.role = role;
    }

    /* Getters and Setters */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public RoleDto getRole() {
        return role;
    }

    public void setRole(RoleDto role) {
        this.role = role;
    }

    public boolean isInitialPassword() {
        return initialPassword;
    }

    public void setInitialPassword(boolean initialPassword) {
        this.initialPassword = initialPassword;
    }

    public List<UserActivityLogDto> getUserActivityLogs() {
        return userActivityLogs;
    }

    public void setUserActivityLogs(List<UserActivityLogDto> userActivityLogs) {
        this.userActivityLogs = userActivityLogs;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userType=" + userType +
                ", userName='" + userName + '\'' +
//                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", role=" + ((role != null) ? role.getName() : "") +
                ", userActivityLogs=" + userActivityLogs +
                '}';
    }
}
